2O                         POINT   r,     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;
struct Globals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _ScreenParams;
    float4 unity_OrthoParams;
    float4 _WorldSpaceLightPos0;
    half4 unity_OcclusionMaskSelector;
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4glstate_matrix_projection[4];
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    float4 unity_ProbeVolumeParams;
    float4 hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[4];
    float3 unity_ProbeVolumeSizeInv;
    float3 unity_ProbeVolumeMin;
    half4 _LightColor0;
    half4 _SpecColor;
    float4 hlslcc_mtx4x4unity_WorldToLight[4];
    half4 _Color;
    half _Shininess;
    half4 _RimColor;
    half _RimMin;
    half _RimMax;
    half4 _HColor;
    half _RampThreshold;
    half _RampSmooth;
    half _SpecSmooth;
    float4 _SketchTex_ST;
    half4 _SketchColor;
    half _SketchHalftoneMin;
    half _SketchHalftoneMax;
};

struct Mtl_FragmentIn
{
    half2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    half4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(0) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant Globals_Type& Globals [[ buffer(0) ]],
    texture2d<half, access::sample > _MainTex [[ texture (0) ]] ,
    sampler sampler_MainTex [[ sampler (0) ]] ,
    texture2d<half, access::sample > _LightTexture0 [[ texture (1) ]] ,
    sampler sampler_LightTexture0 [[ sampler (1) ]] ,
    texture2d<half, access::sample > _SketchTex [[ texture (2) ]] ,
    sampler sampler_SketchTex [[ sampler (2) ]] ,
    texture3d<float, access::sample > unity_ProbeVolumeSH [[ texture (3) ]] ,
    sampler samplerunity_ProbeVolumeSH [[ sampler (3) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half3 u_xlat16_0;
    float3 u_xlat1;
    half u_xlat16_1;
    float3 u_xlat2;
    float3 u_xlat3;
    half4 u_xlat16_3;
    half3 u_xlat16_4;
    float4 u_xlat5;
    half4 u_xlat16_5;
    float3 u_xlat6;
    float3 u_xlat7;
    bool u_xlatb7;
    float2 u_xlat8;
    float2 u_xlat9;
    half3 u_xlat16_10;
    half3 u_xlat16_11;
    half3 u_xlat16_12;
    half3 u_xlat16_13;
    half u_xlat16_14;
    half3 u_xlat16_19;
    float u_xlat20;
    float u_xlat21;
    half u_xlat16_26;
    float u_xlat34;
    float u_xlat35;
    int u_xlati35;
    float u_xlat42;
    float u_xlat43;
    half u_xlat16_43;
    bool u_xlatb43;
    float u_xlat44;
    half u_xlat16_46;
    float u_xlat48;
    int u_xlati48;
    half u_xlat16_52;
    half u_xlat16_53;
    u_xlat0.xyz = (-input.TEXCOORD2.xyz) + Globals._WorldSpaceLightPos0.xyz;
    u_xlat42 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat42 = rsqrt(u_xlat42);
    u_xlat1.xyz = float3(u_xlat42) * u_xlat0.xyz;
    u_xlat2.xyz = (-input.TEXCOORD2.xyz) + Globals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat43 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat43 = rsqrt(u_xlat43);
    u_xlat2.xyz = float3(u_xlat43) * u_xlat2.xyz;
    u_xlat16_3 = _MainTex.sample(sampler_MainTex, float2(input.TEXCOORD0.xy));
    u_xlat16_4.xyz = half3(u_xlat16_3.xyz * Globals._Color.xyz);
    u_xlat16_5.xy = half2(input.TEXCOORD3.xy / input.TEXCOORD3.ww);
    u_xlat43 = Globals._ScreenParams.y / Globals._ScreenParams.x;
    u_xlat6.xyz = Globals.hlslcc_mtx4x4unity_ObjectToWorld[3].yyy * Globals.hlslcc_mtx4x4unity_MatrixV[1].xyz;
    u_xlat6.xyz = Globals.hlslcc_mtx4x4unity_MatrixV[0].xyz * Globals.hlslcc_mtx4x4unity_ObjectToWorld[3].xxx + u_xlat6.xyz;
    u_xlat6.xyz = Globals.hlslcc_mtx4x4unity_MatrixV[2].xyz * Globals.hlslcc_mtx4x4unity_ObjectToWorld[3].zzz + u_xlat6.xyz;
    u_xlat6.xyz = Globals.hlslcc_mtx4x4unity_MatrixV[3].xyz * Globals.hlslcc_mtx4x4unity_ObjectToWorld[3].www + u_xlat6.xyz;
    u_xlat44 = u_xlat43 * (-u_xlat6.x);
    u_xlat44 = u_xlat44 * Globals.hlslcc_mtx4x4glstate_matrix_projection[1].y;
    u_xlat6.x = (-u_xlat6.y) * Globals.hlslcc_mtx4x4glstate_matrix_projection[1].y;
    u_xlat20 = u_xlat43 * Globals._SketchTex_ST.y;
    u_xlat48 = Globals._SketchTex_ST.x * 0.5;
    u_xlat20 = u_xlat20 * 0.5;
    u_xlatb7 = Globals.unity_OrthoParams.w<1.0;
    u_xlat21 = (-Globals._SketchTex_ST.x) * 0.5 + float(u_xlat16_5.x);
    u_xlat35 = float(u_xlat16_5.y) * u_xlat43 + (-u_xlat20);
    u_xlat21 = u_xlat21 * u_xlat6.z + u_xlat48;
    u_xlat34 = u_xlat35 * u_xlat6.z + u_xlat20;
    u_xlat44 = u_xlat44 * u_xlat48;
    u_xlati48 = int((0.0<Globals.hlslcc_mtx4x4glstate_matrix_projection[1].y) ? 0xFFFFFFFFu : 0u);
    u_xlati35 = int((Globals.hlslcc_mtx4x4glstate_matrix_projection[1].y<0.0) ? 0xFFFFFFFFu : 0u);
    u_xlati48 = (-u_xlati48) + u_xlati35;
    u_xlat48 = float(u_xlati48);
    u_xlat8.x = (-u_xlat44) * u_xlat48 + u_xlat21;
    u_xlat6.x = u_xlat20 * u_xlat6.x;
    u_xlat20 = u_xlat48 * u_xlat6.x;
    u_xlat8.y = (-u_xlat6.x) * u_xlat48 + u_xlat34;
    u_xlat9.x = u_xlat44 * u_xlat48 + float(u_xlat16_5.x);
    u_xlat9.y = float(u_xlat16_5.y) * u_xlat43 + u_xlat20;
    u_xlat6.xy = (bool(u_xlatb7)) ? u_xlat8.xy : u_xlat9.xy;
    u_xlat16_46 = half(u_xlat16_3.w * Globals._Shininess);
    u_xlat43 = dot(u_xlat2.xyz, float3(input.TEXCOORD1.xyz));
    u_xlat43 = clamp(u_xlat43, 0.0f, 1.0f);
    u_xlat43 = (-u_xlat43) + 1.0;
    u_xlat16_5.x = half((-Globals._RimMin) + Globals._RimMax);
    u_xlat16_19.x = half(u_xlat43 + (-float(Globals._RimMin)));
    u_xlat16_5.x = half(float(1.0) / float(u_xlat16_5.x));
    u_xlat16_5.x = half(u_xlat16_5.x * u_xlat16_19.x);
    u_xlat16_5.x = clamp(u_xlat16_5.x, 0.0h, 1.0h);
    u_xlat16_19.x = half(float(u_xlat16_5.x) * -2.0 + 3.0);
    u_xlat16_5.x = half(u_xlat16_5.x * u_xlat16_5.x);
    u_xlat16_5.x = half(u_xlat16_5.x * u_xlat16_19.x);
    u_xlat16_5.x = half(u_xlat16_5.x * Globals._RimColor.w);
    u_xlat16_19.xyz = half3((-u_xlat16_3.xyz) * Globals._Color.xyz + Globals._RimColor.xyz);
    u_xlat16_4.xyz = half3(u_xlat16_5.xxx * u_xlat16_19.xyz + u_xlat16_4.xyz);
    u_xlat3.xyz = input.TEXCOORD2.yyy * Globals.hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat3.xyz = Globals.hlslcc_mtx4x4unity_WorldToLight[0].xyz * input.TEXCOORD2.xxx + u_xlat3.xyz;
    u_xlat3.xyz = Globals.hlslcc_mtx4x4unity_WorldToLight[2].xyz * input.TEXCOORD2.zzz + u_xlat3.xyz;
    u_xlat3.xyz = u_xlat3.xyz + Globals.hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlatb43 = Globals.unity_ProbeVolumeParams.x==1.0;
    if(u_xlatb43){
        u_xlatb43 = Globals.unity_ProbeVolumeParams.y==1.0;
        u_xlat7.xyz = input.TEXCOORD2.yyy * Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[1].xyz;
        u_xlat7.xyz = Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[0].xyz * input.TEXCOORD2.xxx + u_xlat7.xyz;
        u_xlat7.xyz = Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[2].xyz * input.TEXCOORD2.zzz + u_xlat7.xyz;
        u_xlat7.xyz = u_xlat7.xyz + Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[3].xyz;
        u_xlat7.xyz = (bool(u_xlatb43)) ? u_xlat7.xyz : input.TEXCOORD2.xyz;
        u_xlat7.xyz = u_xlat7.xyz + (-Globals.unity_ProbeVolumeMin.xyzx.xyz);
        u_xlat5.yzw = u_xlat7.xyz * Globals.unity_ProbeVolumeSizeInv.xyzx.xyz;
        u_xlat43 = u_xlat5.y * 0.25 + 0.75;
        u_xlat44 = Globals.unity_ProbeVolumeParams.z * 0.5 + 0.75;
        u_xlat5.x = max(u_xlat43, u_xlat44);
        u_xlat5 = unity_ProbeVolumeSH.sample(samplerunity_ProbeVolumeSH, u_xlat5.xzw);
        u_xlat16_5 = half4(u_xlat5);
    } else {
        u_xlat16_5.x = half(1.0);
        u_xlat16_5.y = half(1.0);
        u_xlat16_5.z = half(1.0);
        u_xlat16_5.w = half(1.0);
    }
    u_xlat16_10.x = dot(u_xlat16_5, Globals.unity_OcclusionMaskSelector);
    u_xlat16_10.x = clamp(u_xlat16_10.x, 0.0h, 1.0h);
    u_xlat43 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat16_43 = _LightTexture0.sample(sampler_LightTexture0, float2(u_xlat43)).w;
    u_xlat16_43 = half(u_xlat16_10.x * u_xlat16_43);
    u_xlat16_10.xyz = half3(half3(u_xlat16_43) * Globals._LightColor0.xyz);
    u_xlat16_52 = dot(input.TEXCOORD1.xyz, input.TEXCOORD1.xyz);
    u_xlat16_52 = rsqrt(u_xlat16_52);
    u_xlat16_11.xyz = half3(half3(u_xlat16_52) * input.TEXCOORD1.xyz);
    u_xlat16_52 = dot(float3(u_xlat16_11.xyz), u_xlat1.xyz);
    u_xlat16_52 = half(max(float(u_xlat16_52), 0.0));
    u_xlat16_53 = half((-float(Globals._RampSmooth)) * 0.5 + float(Globals._RampThreshold));
    u_xlat16_12.x = half(float(Globals._RampSmooth) * 0.5 + float(Globals._RampThreshold));
    u_xlat16_12.x = half((-u_xlat16_53) + u_xlat16_12.x);
    u_xlat16_52 = half(u_xlat16_52 + (-u_xlat16_53));
    u_xlat16_53 = half(float(1.0) / float(u_xlat16_12.x));
    u_xlat16_52 = half(u_xlat16_52 * u_xlat16_53);
    u_xlat16_52 = clamp(u_xlat16_52, 0.0h, 1.0h);
    u_xlat16_53 = half(float(u_xlat16_52) * -2.0 + 3.0);
    u_xlat16_52 = half(u_xlat16_52 * u_xlat16_52);
    u_xlat16_52 = half(u_xlat16_52 * u_xlat16_53);
    u_xlat16_1 = _SketchTex.sample(sampler_SketchTex, u_xlat6.xy).w;
    u_xlat16_53 = half(float(u_xlat16_1) + -0.200000003);
    u_xlat16_12.x = max(u_xlat16_52, Globals._SketchHalftoneMin);
    u_xlat16_12.x = min(u_xlat16_12.x, Globals._SketchHalftoneMax);
    u_xlat16_26 = half(u_xlat16_1 + (-u_xlat16_53));
    u_xlat16_53 = half((-u_xlat16_53) + u_xlat16_12.x);
    u_xlat16_12.x = half(float(1.0) / float(u_xlat16_26));
    u_xlat16_53 = half(u_xlat16_53 * u_xlat16_12.x);
    u_xlat16_53 = clamp(u_xlat16_53, 0.0h, 1.0h);
    u_xlat16_12.x = half(float(u_xlat16_53) * -2.0 + 3.0);
    u_xlat16_53 = half(u_xlat16_53 * u_xlat16_53);
    u_xlat16_53 = half(u_xlat16_53 * u_xlat16_12.x);
    u_xlat16_12.xyz = half3(half3(u_xlat16_52) * Globals._HColor.xyz);
    u_xlat16_13.xyz = half3(u_xlat0.xyz * float3(u_xlat42) + u_xlat2.xyz);
    u_xlat16_52 = dot(u_xlat16_13.xyz, u_xlat16_13.xyz);
    u_xlat16_52 = rsqrt(u_xlat16_52);
    u_xlat16_13.xyz = half3(half3(u_xlat16_52) * u_xlat16_13.xyz);
    u_xlat16_52 = dot(u_xlat16_11.xyz, u_xlat16_13.xyz);
    u_xlat16_52 = half(max(float(u_xlat16_52), 0.0));
    u_xlat16_46 = half(float(u_xlat16_46) * 128.0);
    u_xlat16_0.x = log2(u_xlat16_52);
    u_xlat16_0.x = half(u_xlat16_0.x * u_xlat16_46);
    u_xlat16_0.x = exp2(u_xlat16_0.x);
    u_xlat16_46 = half((-float(Globals._SpecSmooth)) * 0.5 + 0.5);
    u_xlat16_52 = half(float(Globals._SpecSmooth) * 0.5 + 0.5);
    u_xlat16_14 = half((-u_xlat16_46) + u_xlat16_52);
    u_xlat16_0.x = half(float(u_xlat16_0.x) * 2.0 + (-float(u_xlat16_46)));
    u_xlat16_14 = half(float(1.0) / float(u_xlat16_14));
    u_xlat16_0.x = half(u_xlat16_14 * u_xlat16_0.x);
    u_xlat16_0.x = clamp(u_xlat16_0.x, 0.0h, 1.0h);
    u_xlat16_14 = half(float(u_xlat16_0.x) * -2.0 + 3.0);
    u_xlat16_0.x = half(u_xlat16_0.x * u_xlat16_0.x);
    u_xlat16_0.x = half(u_xlat16_0.x * u_xlat16_14);
    u_xlat16_4.xyz = half3(u_xlat16_4.xyz * u_xlat16_10.xyz);
    u_xlat16_10.xyz = half3(u_xlat16_10.xyz * Globals._SpecColor.xyz);
    u_xlat16_0.xyz = half3(u_xlat16_0.xxx * u_xlat16_10.xyz);
    u_xlat16_0.xyz = half3(u_xlat16_4.xyz * u_xlat16_12.xyz + u_xlat16_0.xyz);
    u_xlat16_4.xyz = half3((-float3(Globals._SketchColor.xyz)) + float3(1.0, 1.0, 1.0));
    u_xlat16_4.xyz = half3(half3(u_xlat16_53) * u_xlat16_4.xyz + Globals._SketchColor.xyz);
    output.SV_Target0.xyz = half3(u_xlat16_0.xyz * u_xlat16_4.xyz);
    output.SV_Target0.w = 1.0;
    return output;
}
                            Globals         _WorldSpaceCameraPos                         _ScreenParams                           unity_OrthoParams                            _WorldSpaceLightPos0                  0      unity_OcclusionMaskSelector                  @      unity_ProbeVolumeParams                        unity_ProbeVolumeSizeInv                  `     unity_ProbeVolumeMin                  p     _LightColor0                 �  
   _SpecColor                   �     _Color                   �  
   _Shininess                   �  	   _RimColor                    �     _RimMin                  �     _RimMax                  �     _HColor                  �     _RampThreshold                   �     _RampSmooth                  �     _SpecSmooth                  �     _SketchTex_ST                           _SketchColor                      _SketchHalftoneMin                        _SketchHalftoneMax                        unity_ObjectToWorld                  P      glstate_matrix_projection                    �      unity_MatrixV                    �      unity_ProbeVolumeWorldToObject                         unity_WorldToLight                   �        _MainTex              _LightTexture0           
   _SketchTex              unity_ProbeVolumeSH             Globals            