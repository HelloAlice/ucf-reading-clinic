﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// TCP2_Demo_PBS/SkyboxSetting[]
struct SkyboxSettingU5BU5D_t1256192879;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCP2_Demo_PBS
struct  TCP2_Demo_PBS_t3170765153  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Light TCP2_Demo_PBS::DirLight
	Light_t494725636 * ___DirLight_2;
	// UnityEngine.GameObject TCP2_Demo_PBS::PointLights
	GameObject_t1756533147 * ___PointLights_3;
	// UnityEngine.MeshRenderer TCP2_Demo_PBS::Robot
	MeshRenderer_t1268241104 * ___Robot_4;
	// UnityEngine.GameObject TCP2_Demo_PBS::Canvas
	GameObject_t1756533147 * ___Canvas_5;
	// TCP2_Demo_PBS/SkyboxSetting[] TCP2_Demo_PBS::SkySettings
	SkyboxSettingU5BU5D_t1256192879* ___SkySettings_6;
	// System.Boolean TCP2_Demo_PBS::FlipLight
	bool ___FlipLight_7;
	// UnityEngine.Texture2D[] TCP2_Demo_PBS::RampTextures
	Texture2DU5BU5D_t2724090252* ___RampTextures_8;
	// UnityEngine.UI.Slider TCP2_Demo_PBS::SmoothnessSlider
	Slider_t297367283 * ___SmoothnessSlider_9;
	// UnityEngine.UI.Text TCP2_Demo_PBS::SmoothnessValue
	Text_t356221433 * ___SmoothnessValue_10;
	// UnityEngine.UI.Slider TCP2_Demo_PBS::MetallicSlider
	Slider_t297367283 * ___MetallicSlider_11;
	// UnityEngine.UI.Text TCP2_Demo_PBS::MetallicValue
	Text_t356221433 * ___MetallicValue_12;
	// UnityEngine.UI.Text TCP2_Demo_PBS::BumpScaleValue
	Text_t356221433 * ___BumpScaleValue_13;
	// UnityEngine.UI.Text TCP2_Demo_PBS::ShaderText
	Text_t356221433 * ___ShaderText_14;
	// UnityEngine.UI.Text TCP2_Demo_PBS::SkyboxValue
	Text_t356221433 * ___SkyboxValue_15;
	// UnityEngine.UI.Text TCP2_Demo_PBS::RampValue
	Text_t356221433 * ___RampValue_16;
	// UnityEngine.UI.Slider TCP2_Demo_PBS::RampThresholdSlider
	Slider_t297367283 * ___RampThresholdSlider_17;
	// UnityEngine.UI.Text TCP2_Demo_PBS::RampThresholdValue
	Text_t356221433 * ___RampThresholdValue_18;
	// UnityEngine.UI.Slider TCP2_Demo_PBS::RampSmoothSlider
	Slider_t297367283 * ___RampSmoothSlider_19;
	// UnityEngine.UI.Text TCP2_Demo_PBS::RampSmoothValue
	Text_t356221433 * ___RampSmoothValue_20;
	// UnityEngine.UI.Slider TCP2_Demo_PBS::RampSmoothAddSlider
	Slider_t297367283 * ___RampSmoothAddSlider_21;
	// UnityEngine.UI.Text TCP2_Demo_PBS::RampSmoothAddValue
	Text_t356221433 * ___RampSmoothAddValue_22;
	// UnityEngine.UI.RawImage TCP2_Demo_PBS::RampImage
	RawImage_t2749640213 * ___RampImage_23;
	// System.Int32 TCP2_Demo_PBS::currentSky
	int32_t ___currentSky_24;
	// System.Int32 TCP2_Demo_PBS::currentRamp
	int32_t ___currentRamp_25;
	// UnityEngine.Material TCP2_Demo_PBS::robotMaterial
	Material_t193706927 * ___robotMaterial_26;
	// System.Boolean TCP2_Demo_PBS::mUseOutline
	bool ___mUseOutline_27;
	// System.Boolean TCP2_Demo_PBS::mRotatePointLights
	bool ___mRotatePointLights_28;

public:
	inline static int32_t get_offset_of_DirLight_2() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___DirLight_2)); }
	inline Light_t494725636 * get_DirLight_2() const { return ___DirLight_2; }
	inline Light_t494725636 ** get_address_of_DirLight_2() { return &___DirLight_2; }
	inline void set_DirLight_2(Light_t494725636 * value)
	{
		___DirLight_2 = value;
		Il2CppCodeGenWriteBarrier(&___DirLight_2, value);
	}

	inline static int32_t get_offset_of_PointLights_3() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___PointLights_3)); }
	inline GameObject_t1756533147 * get_PointLights_3() const { return ___PointLights_3; }
	inline GameObject_t1756533147 ** get_address_of_PointLights_3() { return &___PointLights_3; }
	inline void set_PointLights_3(GameObject_t1756533147 * value)
	{
		___PointLights_3 = value;
		Il2CppCodeGenWriteBarrier(&___PointLights_3, value);
	}

	inline static int32_t get_offset_of_Robot_4() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___Robot_4)); }
	inline MeshRenderer_t1268241104 * get_Robot_4() const { return ___Robot_4; }
	inline MeshRenderer_t1268241104 ** get_address_of_Robot_4() { return &___Robot_4; }
	inline void set_Robot_4(MeshRenderer_t1268241104 * value)
	{
		___Robot_4 = value;
		Il2CppCodeGenWriteBarrier(&___Robot_4, value);
	}

	inline static int32_t get_offset_of_Canvas_5() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___Canvas_5)); }
	inline GameObject_t1756533147 * get_Canvas_5() const { return ___Canvas_5; }
	inline GameObject_t1756533147 ** get_address_of_Canvas_5() { return &___Canvas_5; }
	inline void set_Canvas_5(GameObject_t1756533147 * value)
	{
		___Canvas_5 = value;
		Il2CppCodeGenWriteBarrier(&___Canvas_5, value);
	}

	inline static int32_t get_offset_of_SkySettings_6() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___SkySettings_6)); }
	inline SkyboxSettingU5BU5D_t1256192879* get_SkySettings_6() const { return ___SkySettings_6; }
	inline SkyboxSettingU5BU5D_t1256192879** get_address_of_SkySettings_6() { return &___SkySettings_6; }
	inline void set_SkySettings_6(SkyboxSettingU5BU5D_t1256192879* value)
	{
		___SkySettings_6 = value;
		Il2CppCodeGenWriteBarrier(&___SkySettings_6, value);
	}

	inline static int32_t get_offset_of_FlipLight_7() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___FlipLight_7)); }
	inline bool get_FlipLight_7() const { return ___FlipLight_7; }
	inline bool* get_address_of_FlipLight_7() { return &___FlipLight_7; }
	inline void set_FlipLight_7(bool value)
	{
		___FlipLight_7 = value;
	}

	inline static int32_t get_offset_of_RampTextures_8() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___RampTextures_8)); }
	inline Texture2DU5BU5D_t2724090252* get_RampTextures_8() const { return ___RampTextures_8; }
	inline Texture2DU5BU5D_t2724090252** get_address_of_RampTextures_8() { return &___RampTextures_8; }
	inline void set_RampTextures_8(Texture2DU5BU5D_t2724090252* value)
	{
		___RampTextures_8 = value;
		Il2CppCodeGenWriteBarrier(&___RampTextures_8, value);
	}

	inline static int32_t get_offset_of_SmoothnessSlider_9() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___SmoothnessSlider_9)); }
	inline Slider_t297367283 * get_SmoothnessSlider_9() const { return ___SmoothnessSlider_9; }
	inline Slider_t297367283 ** get_address_of_SmoothnessSlider_9() { return &___SmoothnessSlider_9; }
	inline void set_SmoothnessSlider_9(Slider_t297367283 * value)
	{
		___SmoothnessSlider_9 = value;
		Il2CppCodeGenWriteBarrier(&___SmoothnessSlider_9, value);
	}

	inline static int32_t get_offset_of_SmoothnessValue_10() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___SmoothnessValue_10)); }
	inline Text_t356221433 * get_SmoothnessValue_10() const { return ___SmoothnessValue_10; }
	inline Text_t356221433 ** get_address_of_SmoothnessValue_10() { return &___SmoothnessValue_10; }
	inline void set_SmoothnessValue_10(Text_t356221433 * value)
	{
		___SmoothnessValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___SmoothnessValue_10, value);
	}

	inline static int32_t get_offset_of_MetallicSlider_11() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___MetallicSlider_11)); }
	inline Slider_t297367283 * get_MetallicSlider_11() const { return ___MetallicSlider_11; }
	inline Slider_t297367283 ** get_address_of_MetallicSlider_11() { return &___MetallicSlider_11; }
	inline void set_MetallicSlider_11(Slider_t297367283 * value)
	{
		___MetallicSlider_11 = value;
		Il2CppCodeGenWriteBarrier(&___MetallicSlider_11, value);
	}

	inline static int32_t get_offset_of_MetallicValue_12() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___MetallicValue_12)); }
	inline Text_t356221433 * get_MetallicValue_12() const { return ___MetallicValue_12; }
	inline Text_t356221433 ** get_address_of_MetallicValue_12() { return &___MetallicValue_12; }
	inline void set_MetallicValue_12(Text_t356221433 * value)
	{
		___MetallicValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___MetallicValue_12, value);
	}

	inline static int32_t get_offset_of_BumpScaleValue_13() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___BumpScaleValue_13)); }
	inline Text_t356221433 * get_BumpScaleValue_13() const { return ___BumpScaleValue_13; }
	inline Text_t356221433 ** get_address_of_BumpScaleValue_13() { return &___BumpScaleValue_13; }
	inline void set_BumpScaleValue_13(Text_t356221433 * value)
	{
		___BumpScaleValue_13 = value;
		Il2CppCodeGenWriteBarrier(&___BumpScaleValue_13, value);
	}

	inline static int32_t get_offset_of_ShaderText_14() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___ShaderText_14)); }
	inline Text_t356221433 * get_ShaderText_14() const { return ___ShaderText_14; }
	inline Text_t356221433 ** get_address_of_ShaderText_14() { return &___ShaderText_14; }
	inline void set_ShaderText_14(Text_t356221433 * value)
	{
		___ShaderText_14 = value;
		Il2CppCodeGenWriteBarrier(&___ShaderText_14, value);
	}

	inline static int32_t get_offset_of_SkyboxValue_15() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___SkyboxValue_15)); }
	inline Text_t356221433 * get_SkyboxValue_15() const { return ___SkyboxValue_15; }
	inline Text_t356221433 ** get_address_of_SkyboxValue_15() { return &___SkyboxValue_15; }
	inline void set_SkyboxValue_15(Text_t356221433 * value)
	{
		___SkyboxValue_15 = value;
		Il2CppCodeGenWriteBarrier(&___SkyboxValue_15, value);
	}

	inline static int32_t get_offset_of_RampValue_16() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___RampValue_16)); }
	inline Text_t356221433 * get_RampValue_16() const { return ___RampValue_16; }
	inline Text_t356221433 ** get_address_of_RampValue_16() { return &___RampValue_16; }
	inline void set_RampValue_16(Text_t356221433 * value)
	{
		___RampValue_16 = value;
		Il2CppCodeGenWriteBarrier(&___RampValue_16, value);
	}

	inline static int32_t get_offset_of_RampThresholdSlider_17() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___RampThresholdSlider_17)); }
	inline Slider_t297367283 * get_RampThresholdSlider_17() const { return ___RampThresholdSlider_17; }
	inline Slider_t297367283 ** get_address_of_RampThresholdSlider_17() { return &___RampThresholdSlider_17; }
	inline void set_RampThresholdSlider_17(Slider_t297367283 * value)
	{
		___RampThresholdSlider_17 = value;
		Il2CppCodeGenWriteBarrier(&___RampThresholdSlider_17, value);
	}

	inline static int32_t get_offset_of_RampThresholdValue_18() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___RampThresholdValue_18)); }
	inline Text_t356221433 * get_RampThresholdValue_18() const { return ___RampThresholdValue_18; }
	inline Text_t356221433 ** get_address_of_RampThresholdValue_18() { return &___RampThresholdValue_18; }
	inline void set_RampThresholdValue_18(Text_t356221433 * value)
	{
		___RampThresholdValue_18 = value;
		Il2CppCodeGenWriteBarrier(&___RampThresholdValue_18, value);
	}

	inline static int32_t get_offset_of_RampSmoothSlider_19() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___RampSmoothSlider_19)); }
	inline Slider_t297367283 * get_RampSmoothSlider_19() const { return ___RampSmoothSlider_19; }
	inline Slider_t297367283 ** get_address_of_RampSmoothSlider_19() { return &___RampSmoothSlider_19; }
	inline void set_RampSmoothSlider_19(Slider_t297367283 * value)
	{
		___RampSmoothSlider_19 = value;
		Il2CppCodeGenWriteBarrier(&___RampSmoothSlider_19, value);
	}

	inline static int32_t get_offset_of_RampSmoothValue_20() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___RampSmoothValue_20)); }
	inline Text_t356221433 * get_RampSmoothValue_20() const { return ___RampSmoothValue_20; }
	inline Text_t356221433 ** get_address_of_RampSmoothValue_20() { return &___RampSmoothValue_20; }
	inline void set_RampSmoothValue_20(Text_t356221433 * value)
	{
		___RampSmoothValue_20 = value;
		Il2CppCodeGenWriteBarrier(&___RampSmoothValue_20, value);
	}

	inline static int32_t get_offset_of_RampSmoothAddSlider_21() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___RampSmoothAddSlider_21)); }
	inline Slider_t297367283 * get_RampSmoothAddSlider_21() const { return ___RampSmoothAddSlider_21; }
	inline Slider_t297367283 ** get_address_of_RampSmoothAddSlider_21() { return &___RampSmoothAddSlider_21; }
	inline void set_RampSmoothAddSlider_21(Slider_t297367283 * value)
	{
		___RampSmoothAddSlider_21 = value;
		Il2CppCodeGenWriteBarrier(&___RampSmoothAddSlider_21, value);
	}

	inline static int32_t get_offset_of_RampSmoothAddValue_22() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___RampSmoothAddValue_22)); }
	inline Text_t356221433 * get_RampSmoothAddValue_22() const { return ___RampSmoothAddValue_22; }
	inline Text_t356221433 ** get_address_of_RampSmoothAddValue_22() { return &___RampSmoothAddValue_22; }
	inline void set_RampSmoothAddValue_22(Text_t356221433 * value)
	{
		___RampSmoothAddValue_22 = value;
		Il2CppCodeGenWriteBarrier(&___RampSmoothAddValue_22, value);
	}

	inline static int32_t get_offset_of_RampImage_23() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___RampImage_23)); }
	inline RawImage_t2749640213 * get_RampImage_23() const { return ___RampImage_23; }
	inline RawImage_t2749640213 ** get_address_of_RampImage_23() { return &___RampImage_23; }
	inline void set_RampImage_23(RawImage_t2749640213 * value)
	{
		___RampImage_23 = value;
		Il2CppCodeGenWriteBarrier(&___RampImage_23, value);
	}

	inline static int32_t get_offset_of_currentSky_24() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___currentSky_24)); }
	inline int32_t get_currentSky_24() const { return ___currentSky_24; }
	inline int32_t* get_address_of_currentSky_24() { return &___currentSky_24; }
	inline void set_currentSky_24(int32_t value)
	{
		___currentSky_24 = value;
	}

	inline static int32_t get_offset_of_currentRamp_25() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___currentRamp_25)); }
	inline int32_t get_currentRamp_25() const { return ___currentRamp_25; }
	inline int32_t* get_address_of_currentRamp_25() { return &___currentRamp_25; }
	inline void set_currentRamp_25(int32_t value)
	{
		___currentRamp_25 = value;
	}

	inline static int32_t get_offset_of_robotMaterial_26() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___robotMaterial_26)); }
	inline Material_t193706927 * get_robotMaterial_26() const { return ___robotMaterial_26; }
	inline Material_t193706927 ** get_address_of_robotMaterial_26() { return &___robotMaterial_26; }
	inline void set_robotMaterial_26(Material_t193706927 * value)
	{
		___robotMaterial_26 = value;
		Il2CppCodeGenWriteBarrier(&___robotMaterial_26, value);
	}

	inline static int32_t get_offset_of_mUseOutline_27() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___mUseOutline_27)); }
	inline bool get_mUseOutline_27() const { return ___mUseOutline_27; }
	inline bool* get_address_of_mUseOutline_27() { return &___mUseOutline_27; }
	inline void set_mUseOutline_27(bool value)
	{
		___mUseOutline_27 = value;
	}

	inline static int32_t get_offset_of_mRotatePointLights_28() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_t3170765153, ___mRotatePointLights_28)); }
	inline bool get_mRotatePointLights_28() const { return ___mRotatePointLights_28; }
	inline bool* get_address_of_mRotatePointLights_28() { return &___mRotatePointLights_28; }
	inline void set_mRotatePointLights_28(bool value)
	{
		___mRotatePointLights_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
