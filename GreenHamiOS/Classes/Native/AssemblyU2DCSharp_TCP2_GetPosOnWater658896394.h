﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Material
struct Material_t193706927;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCP2_GetPosOnWater
struct  TCP2_GetPosOnWater_t658896394  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material TCP2_GetPosOnWater::WaterMaterial
	Material_t193706927 * ___WaterMaterial_2;
	// System.Boolean TCP2_GetPosOnWater::followWaterHeight
	bool ___followWaterHeight_3;
	// System.Single TCP2_GetPosOnWater::heightOffset
	float ___heightOffset_4;
	// System.Single TCP2_GetPosOnWater::heightScale
	float ___heightScale_5;
	// System.Boolean TCP2_GetPosOnWater::isValid
	bool ___isValid_6;
	// System.Int32 TCP2_GetPosOnWater::sineCount
	int32_t ___sineCount_7;
	// System.Single[] TCP2_GetPosOnWater::sinePosOffsetsX
	SingleU5BU5D_t577127397* ___sinePosOffsetsX_8;
	// System.Single[] TCP2_GetPosOnWater::sinePosOffsetsZ
	SingleU5BU5D_t577127397* ___sinePosOffsetsZ_9;
	// System.Single[] TCP2_GetPosOnWater::sinePhsOffsetsX
	SingleU5BU5D_t577127397* ___sinePhsOffsetsX_10;
	// System.Single[] TCP2_GetPosOnWater::sinePhsOffsetsZ
	SingleU5BU5D_t577127397* ___sinePhsOffsetsZ_11;

public:
	inline static int32_t get_offset_of_WaterMaterial_2() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___WaterMaterial_2)); }
	inline Material_t193706927 * get_WaterMaterial_2() const { return ___WaterMaterial_2; }
	inline Material_t193706927 ** get_address_of_WaterMaterial_2() { return &___WaterMaterial_2; }
	inline void set_WaterMaterial_2(Material_t193706927 * value)
	{
		___WaterMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___WaterMaterial_2, value);
	}

	inline static int32_t get_offset_of_followWaterHeight_3() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___followWaterHeight_3)); }
	inline bool get_followWaterHeight_3() const { return ___followWaterHeight_3; }
	inline bool* get_address_of_followWaterHeight_3() { return &___followWaterHeight_3; }
	inline void set_followWaterHeight_3(bool value)
	{
		___followWaterHeight_3 = value;
	}

	inline static int32_t get_offset_of_heightOffset_4() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___heightOffset_4)); }
	inline float get_heightOffset_4() const { return ___heightOffset_4; }
	inline float* get_address_of_heightOffset_4() { return &___heightOffset_4; }
	inline void set_heightOffset_4(float value)
	{
		___heightOffset_4 = value;
	}

	inline static int32_t get_offset_of_heightScale_5() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___heightScale_5)); }
	inline float get_heightScale_5() const { return ___heightScale_5; }
	inline float* get_address_of_heightScale_5() { return &___heightScale_5; }
	inline void set_heightScale_5(float value)
	{
		___heightScale_5 = value;
	}

	inline static int32_t get_offset_of_isValid_6() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___isValid_6)); }
	inline bool get_isValid_6() const { return ___isValid_6; }
	inline bool* get_address_of_isValid_6() { return &___isValid_6; }
	inline void set_isValid_6(bool value)
	{
		___isValid_6 = value;
	}

	inline static int32_t get_offset_of_sineCount_7() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___sineCount_7)); }
	inline int32_t get_sineCount_7() const { return ___sineCount_7; }
	inline int32_t* get_address_of_sineCount_7() { return &___sineCount_7; }
	inline void set_sineCount_7(int32_t value)
	{
		___sineCount_7 = value;
	}

	inline static int32_t get_offset_of_sinePosOffsetsX_8() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___sinePosOffsetsX_8)); }
	inline SingleU5BU5D_t577127397* get_sinePosOffsetsX_8() const { return ___sinePosOffsetsX_8; }
	inline SingleU5BU5D_t577127397** get_address_of_sinePosOffsetsX_8() { return &___sinePosOffsetsX_8; }
	inline void set_sinePosOffsetsX_8(SingleU5BU5D_t577127397* value)
	{
		___sinePosOffsetsX_8 = value;
		Il2CppCodeGenWriteBarrier(&___sinePosOffsetsX_8, value);
	}

	inline static int32_t get_offset_of_sinePosOffsetsZ_9() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___sinePosOffsetsZ_9)); }
	inline SingleU5BU5D_t577127397* get_sinePosOffsetsZ_9() const { return ___sinePosOffsetsZ_9; }
	inline SingleU5BU5D_t577127397** get_address_of_sinePosOffsetsZ_9() { return &___sinePosOffsetsZ_9; }
	inline void set_sinePosOffsetsZ_9(SingleU5BU5D_t577127397* value)
	{
		___sinePosOffsetsZ_9 = value;
		Il2CppCodeGenWriteBarrier(&___sinePosOffsetsZ_9, value);
	}

	inline static int32_t get_offset_of_sinePhsOffsetsX_10() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___sinePhsOffsetsX_10)); }
	inline SingleU5BU5D_t577127397* get_sinePhsOffsetsX_10() const { return ___sinePhsOffsetsX_10; }
	inline SingleU5BU5D_t577127397** get_address_of_sinePhsOffsetsX_10() { return &___sinePhsOffsetsX_10; }
	inline void set_sinePhsOffsetsX_10(SingleU5BU5D_t577127397* value)
	{
		___sinePhsOffsetsX_10 = value;
		Il2CppCodeGenWriteBarrier(&___sinePhsOffsetsX_10, value);
	}

	inline static int32_t get_offset_of_sinePhsOffsetsZ_11() { return static_cast<int32_t>(offsetof(TCP2_GetPosOnWater_t658896394, ___sinePhsOffsetsZ_11)); }
	inline SingleU5BU5D_t577127397* get_sinePhsOffsetsZ_11() const { return ___sinePhsOffsetsZ_11; }
	inline SingleU5BU5D_t577127397** get_address_of_sinePhsOffsetsZ_11() { return &___sinePhsOffsetsZ_11; }
	inline void set_sinePhsOffsetsZ_11(SingleU5BU5D_t577127397* value)
	{
		___sinePhsOffsetsZ_11 = value;
		Il2CppCodeGenWriteBarrier(&___sinePhsOffsetsZ_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
