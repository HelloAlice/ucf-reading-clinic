﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Stora3897282321.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle4061728485.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle3506117492.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaMacros1884408435.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra1329355276.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer2933102835.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Fp1598668988.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4106934884.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4137084396.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec829768013.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi2617831468.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Ren804170727.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3083157244.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil1916387570.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceUtilities4096327849.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder1347637805.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_InitStat4409649.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Updat1473252352.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Filte3082493643.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe1958726506.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTracker89845299.h"
#include "Vuforia_UnityExtensions_Vuforia_SimpleTargetData3993525265.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour3993660444.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSource2832298792.h"
#include "Vuforia_UnityExtensions_Vuforia_Tracker189438242.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager308318605.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705.h"
#include "Vuforia_UnityExtensions_Vuforia_UserDefinedTargetB3589690572.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst395384314.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundMan3515346924.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton3703236737.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton_Sens1678924861.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamARController2804466264.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager1585193471.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList1278495262.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2396922556.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibrat626398268.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe1486305137.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDet978476007.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM3179336627.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD1524870173.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1333959294.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2981963041.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1414739712.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInput621514313.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp1441575871.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2688375492.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3572864619.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3709210170.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneIn70867863.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone2680906638.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInput2561058385.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2336171397.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DR3236822917.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRayc249603239.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (StorageType_t3897282321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[4] = 
{
	StorageType_t3897282321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (VuforiaARController_t4061728485), -1, sizeof(VuforiaARController_t4061728485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1801[39] = 
{
	VuforiaARController_t4061728485::get_offset_of_CameraDeviceModeSetting_1(),
	VuforiaARController_t4061728485::get_offset_of_MaxSimultaneousImageTargets_2(),
	VuforiaARController_t4061728485::get_offset_of_MaxSimultaneousObjectTargets_3(),
	VuforiaARController_t4061728485::get_offset_of_UseDelayedLoadingObjectTargets_4(),
	VuforiaARController_t4061728485::get_offset_of_CameraDirection_5(),
	VuforiaARController_t4061728485::get_offset_of_MirrorVideoBackground_6(),
	VuforiaARController_t4061728485::get_offset_of_mWorldCenterMode_7(),
	VuforiaARController_t4061728485::get_offset_of_mWorldCenter_8(),
	VuforiaARController_t4061728485::get_offset_of_mTrackerEventHandlers_9(),
	VuforiaARController_t4061728485::get_offset_of_mVideoBgEventHandlers_10(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaInitialized_11(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaStarted_12(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaDeinitialized_13(),
	VuforiaARController_t4061728485::get_offset_of_mOnTrackablesUpdated_14(),
	VuforiaARController_t4061728485::get_offset_of_mRenderOnUpdate_15(),
	VuforiaARController_t4061728485::get_offset_of_mOnPause_16(),
	VuforiaARController_t4061728485::get_offset_of_mPaused_17(),
	VuforiaARController_t4061728485::get_offset_of_mOnBackgroundTextureChanged_18(),
	VuforiaARController_t4061728485::get_offset_of_mStartHasBeenInvoked_19(),
	VuforiaARController_t4061728485::get_offset_of_mHasStarted_20(),
	VuforiaARController_t4061728485::get_offset_of_mBackgroundTextureHasChanged_21(),
	VuforiaARController_t4061728485::get_offset_of_mCameraConfiguration_22(),
	VuforiaARController_t4061728485::get_offset_of_mEyewearBehaviour_23(),
	VuforiaARController_t4061728485::get_offset_of_mVideoBackgroundMgr_24(),
	VuforiaARController_t4061728485::get_offset_of_mCheckStopCamera_25(),
	VuforiaARController_t4061728485::get_offset_of_mClearMaterial_26(),
	VuforiaARController_t4061728485::get_offset_of_mMetalRendering_27(),
	VuforiaARController_t4061728485::get_offset_of_mHasStartedOnce_28(),
	VuforiaARController_t4061728485::get_offset_of_mWasEnabledBeforePause_29(),
	VuforiaARController_t4061728485::get_offset_of_mObjectTrackerWasActiveBeforePause_30(),
	VuforiaARController_t4061728485::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_31(),
	VuforiaARController_t4061728485::get_offset_of_mLastUpdatedFrame_32(),
	VuforiaARController_t4061728485::get_offset_of_mTrackersRequestedToDeinit_33(),
	VuforiaARController_t4061728485::get_offset_of_mMissedToApplyLeftProjectionMatrix_34(),
	VuforiaARController_t4061728485::get_offset_of_mMissedToApplyRightProjectionMatrix_35(),
	VuforiaARController_t4061728485::get_offset_of_mLeftProjectMatrixToApply_36(),
	VuforiaARController_t4061728485::get_offset_of_mRightProjectMatrixToApply_37(),
	VuforiaARController_t4061728485_StaticFields::get_offset_of_mInstance_38(),
	VuforiaARController_t4061728485_StaticFields::get_offset_of_mPadlock_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (WorldCenterMode_t3506117492)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1802[5] = 
{
	WorldCenterMode_t3506117492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (VuforiaMacros_t1884408435)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t1884408435 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1803[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (VuforiaManager_t2424874861), -1, sizeof(VuforiaManager_t2424874861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1804[1] = 
{
	VuforiaManager_t2424874861_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (TrackableIdPair_t1329355276)+ sizeof (Il2CppObject), sizeof(TrackableIdPair_t1329355276 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[2] = 
{
	TrackableIdPair_t1329355276::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableIdPair_t1329355276::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (VuforiaRenderer_t2933102835), -1, sizeof(VuforiaRenderer_t2933102835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1806[1] = 
{
	VuforiaRenderer_t2933102835_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (FpsHint_t1598668988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1807[5] = 
{
	FpsHint_t1598668988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (VideoBackgroundReflection_t4106934884)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1808[4] = 
{
	VideoBackgroundReflection_t4106934884::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (VideoBGCfgData_t4137084396)+ sizeof (Il2CppObject), sizeof(VideoBGCfgData_t4137084396 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[4] = 
{
	VideoBGCfgData_t4137084396::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_reflectionInteger_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (Vec2I_t829768013)+ sizeof (Il2CppObject), sizeof(Vec2I_t829768013 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[2] = 
{
	Vec2I_t829768013::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vec2I_t829768013::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (VideoTextureInfo_t2617831468)+ sizeof (Il2CppObject), sizeof(VideoTextureInfo_t2617831468 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1811[2] = 
{
	VideoTextureInfo_t2617831468::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoTextureInfo_t2617831468::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (RendererAPI_t804170727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1812[5] = 
{
	RendererAPI_t804170727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (VuforiaRuntimeUtilities_t3083157244), -1, sizeof(VuforiaRuntimeUtilities_t3083157244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1813[2] = 
{
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (InitializableBool_t1916387570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1814[4] = 
{
	InitializableBool_t1916387570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (SurfaceUtilities_t4096327849), -1, sizeof(SurfaceUtilities_t4096327849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1815[1] = 
{
	SurfaceUtilities_t4096327849_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (TargetFinder_t1347637805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (InitState_t4409649)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1817[6] = 
{
	InitState_t4409649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (UpdateState_t1473252352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1818[12] = 
{
	UpdateState_t1473252352::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (FilterMode_t3082493643)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1819[3] = 
{
	FilterMode_t3082493643::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (TargetSearchResult_t1958726506)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t1958726506_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1820[6] = 
{
	TargetSearchResult_t1958726506::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (TextRecoAbstractBehaviour_t2386081773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[12] = 
{
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (TextTracker_t89845299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (SimpleTargetData_t3993525265)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t3993525265 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1823[2] = 
{
	SimpleTargetData_t3993525265::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t3993525265::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (TrackableBehaviour_t1779888572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[8] = 
{
	TrackableBehaviour_t1779888572::get_offset_of_U3CTimeStampU3Ek__BackingField_2(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableName_3(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreserveChildSize_4(),
	TrackableBehaviour_t1779888572::get_offset_of_mInitializedInEditor_5(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreviousScale_6(),
	TrackableBehaviour_t1779888572::get_offset_of_mStatus_7(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackable_8(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableEventHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (Status_t4057911311)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1826[7] = 
{
	Status_t4057911311::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (CoordinateSystem_t3993660444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1827[4] = 
{
	CoordinateSystem_t3993660444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (TrackableSource_t2832298792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (Tracker_t189438242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[1] = 
{
	Tracker_t189438242::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (TrackerManager_t308318605), -1, sizeof(TrackerManager_t308318605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1830[1] = 
{
	TrackerManager_t308318605_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (TurnOffAbstractBehaviour_t4084926705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t3589690572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (VideoBackgroundAbstractBehaviour_t395384314), -1, sizeof(VideoBackgroundAbstractBehaviour_t395384314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1833[12] = 
{
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaARController_4(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mResetMatrix_10(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaFrustumSkew_11(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCenterToEyeAxis_12(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mDisabledMeshRenderers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (VideoBackgroundManager_t3515346924), -1, sizeof(VideoBackgroundManager_t3515346924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1834[8] = 
{
	VideoBackgroundManager_t3515346924::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_t3515346924::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_t3515346924::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_t3515346924::get_offset_of_mTexture_4(),
	VideoBackgroundManager_t3515346924::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_t3515346924::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (VirtualButton_t3703236737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (Sensitivity_t1678924861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1836[4] = 
{
	Sensitivity_t1678924861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (VirtualButtonAbstractBehaviour_t2478279366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[15] = 
{
	0,
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviousSensitivity_9(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviouslyEnabled_10(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPressed_11(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHandlers_12(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mLeftTop_13(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mRightBottom_14(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mUnregisterOnDestroy_15(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mVirtualButton_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (WebCamARController_t2804466264), -1, sizeof(WebCamARController_t2804466264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1838[6] = 
{
	WebCamARController_t2804466264::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t2804466264::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t2804466264::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t2804466264::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mInstance_5(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (WordManager_t1585193471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (WordResult_t1915507197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (WordTemplateMode_t1097144495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1842[3] = 
{
	WordTemplateMode_t1097144495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (WordAbstractBehaviour_t2878458725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[3] = 
{
	WordAbstractBehaviour_t2878458725::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (WordFilterMode_t695600879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1844[4] = 
{
	WordFilterMode_t695600879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (WordList_t1278495262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (EyewearCalibrationProfileManager_t2396922556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (EyewearUserCalibrator_t626398268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1848[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (__StaticArrayInitTypeSizeU3D24_t978476007)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t978476007 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1851[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1870[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_Paused_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1874[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1875[36] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1877[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (RaycasterManager_t3179336627), -1, sizeof(RaycasterManager_t3179336627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1878[1] = 
{
	RaycasterManager_t3179336627_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (RaycastResult_t21186376)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[10] = 
{
	RaycastResult_t21186376::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (UIBehaviour_t3960014691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (AxisEventData_t1524870173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[2] = 
{
	AxisEventData_t1524870173::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t1524870173::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (AbstractEventData_t1333959294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[1] = 
{
	AbstractEventData_t1333959294::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (BaseEventData_t2681005625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[1] = 
{
	BaseEventData_t2681005625::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (PointerEventData_t1599784723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[21] = 
{
	PointerEventData_t1599784723::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t1599784723::get_offset_of_m_PointerPress_3(),
	PointerEventData_t1599784723::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t1599784723::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t1599784723::get_offset_of_hovered_9(),
	PointerEventData_t1599784723::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1599784723::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1599784723::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1599784723::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1599784723::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1599784723::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1599784723::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1599784723::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1599784723::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1599784723::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1599784723::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1599784723::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (InputButton_t2981963041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[4] = 
{
	InputButton_t2981963041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (FramePressState_t1414739712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1886[5] = 
{
	FramePressState_t1414739712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (BaseInput_t621514313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (BaseInputModule_t1295781545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[6] = 
{
	BaseInputModule_t1295781545::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t1295781545::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t1295781545::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t1295781545::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t1295781545::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t1295781545::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (PointerInputModule_t1441575871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t1441575871::get_offset_of_m_PointerData_12(),
	PointerInputModule_t1441575871::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (ButtonState_t2688375492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[2] = 
{
	ButtonState_t2688375492::get_offset_of_m_Button_0(),
	ButtonState_t2688375492::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (MouseState_t3572864619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[1] = 
{
	MouseState_t3572864619::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (MouseButtonEventData_t3709210170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[2] = 
{
	MouseButtonEventData_t3709210170::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3709210170::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (StandaloneInputModule_t70867863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[13] = 
{
	StandaloneInputModule_t70867863::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t70867863::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t70867863::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t70867863::get_offset_of_m_CurrentFocusedGameObject_19(),
	StandaloneInputModule_t70867863::get_offset_of_m_HorizontalAxis_20(),
	StandaloneInputModule_t70867863::get_offset_of_m_VerticalAxis_21(),
	StandaloneInputModule_t70867863::get_offset_of_m_SubmitButton_22(),
	StandaloneInputModule_t70867863::get_offset_of_m_CancelButton_23(),
	StandaloneInputModule_t70867863::get_offset_of_m_InputActionsPerSecond_24(),
	StandaloneInputModule_t70867863::get_offset_of_m_RepeatDelay_25(),
	StandaloneInputModule_t70867863::get_offset_of_m_ForceModuleActive_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (InputMode_t2680906638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1894[3] = 
{
	InputMode_t2680906638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (TouchInputModule_t2561058385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[3] = 
{
	TouchInputModule_t2561058385::get_offset_of_m_LastMousePosition_14(),
	TouchInputModule_t2561058385::get_offset_of_m_MousePosition_15(),
	TouchInputModule_t2561058385::get_offset_of_m_ForceModuleActive_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (BaseRaycaster_t2336171397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (Physics2DRaycaster_t3236822917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (PhysicsRaycaster_t249603239), -1, sizeof(PhysicsRaycaster_t249603239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1898[4] = 
{
	0,
	PhysicsRaycaster_t249603239::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t249603239::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t249603239_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
