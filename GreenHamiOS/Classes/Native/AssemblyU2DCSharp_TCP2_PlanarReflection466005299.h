﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCP2_PlanarReflection
struct  TCP2_PlanarReflection_t466005299  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TCP2_PlanarReflection::m_DisablePixelLights
	bool ___m_DisablePixelLights_2;
	// System.Int32 TCP2_PlanarReflection::m_TextureSize
	int32_t ___m_TextureSize_3;
	// System.Single TCP2_PlanarReflection::m_ClipPlaneOffset
	float ___m_ClipPlaneOffset_4;
	// UnityEngine.LayerMask TCP2_PlanarReflection::m_ReflectLayers
	LayerMask_t3188175821  ___m_ReflectLayers_5;
	// System.Collections.Hashtable TCP2_PlanarReflection::m_ReflectionCameras
	Hashtable_t909839986 * ___m_ReflectionCameras_6;
	// UnityEngine.RenderTexture TCP2_PlanarReflection::m_ReflectionTexture
	RenderTexture_t2666733923 * ___m_ReflectionTexture_7;
	// System.Int32 TCP2_PlanarReflection::m_OldReflectionTextureSize
	int32_t ___m_OldReflectionTextureSize_8;

public:
	inline static int32_t get_offset_of_m_DisablePixelLights_2() { return static_cast<int32_t>(offsetof(TCP2_PlanarReflection_t466005299, ___m_DisablePixelLights_2)); }
	inline bool get_m_DisablePixelLights_2() const { return ___m_DisablePixelLights_2; }
	inline bool* get_address_of_m_DisablePixelLights_2() { return &___m_DisablePixelLights_2; }
	inline void set_m_DisablePixelLights_2(bool value)
	{
		___m_DisablePixelLights_2 = value;
	}

	inline static int32_t get_offset_of_m_TextureSize_3() { return static_cast<int32_t>(offsetof(TCP2_PlanarReflection_t466005299, ___m_TextureSize_3)); }
	inline int32_t get_m_TextureSize_3() const { return ___m_TextureSize_3; }
	inline int32_t* get_address_of_m_TextureSize_3() { return &___m_TextureSize_3; }
	inline void set_m_TextureSize_3(int32_t value)
	{
		___m_TextureSize_3 = value;
	}

	inline static int32_t get_offset_of_m_ClipPlaneOffset_4() { return static_cast<int32_t>(offsetof(TCP2_PlanarReflection_t466005299, ___m_ClipPlaneOffset_4)); }
	inline float get_m_ClipPlaneOffset_4() const { return ___m_ClipPlaneOffset_4; }
	inline float* get_address_of_m_ClipPlaneOffset_4() { return &___m_ClipPlaneOffset_4; }
	inline void set_m_ClipPlaneOffset_4(float value)
	{
		___m_ClipPlaneOffset_4 = value;
	}

	inline static int32_t get_offset_of_m_ReflectLayers_5() { return static_cast<int32_t>(offsetof(TCP2_PlanarReflection_t466005299, ___m_ReflectLayers_5)); }
	inline LayerMask_t3188175821  get_m_ReflectLayers_5() const { return ___m_ReflectLayers_5; }
	inline LayerMask_t3188175821 * get_address_of_m_ReflectLayers_5() { return &___m_ReflectLayers_5; }
	inline void set_m_ReflectLayers_5(LayerMask_t3188175821  value)
	{
		___m_ReflectLayers_5 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCameras_6() { return static_cast<int32_t>(offsetof(TCP2_PlanarReflection_t466005299, ___m_ReflectionCameras_6)); }
	inline Hashtable_t909839986 * get_m_ReflectionCameras_6() const { return ___m_ReflectionCameras_6; }
	inline Hashtable_t909839986 ** get_address_of_m_ReflectionCameras_6() { return &___m_ReflectionCameras_6; }
	inline void set_m_ReflectionCameras_6(Hashtable_t909839986 * value)
	{
		___m_ReflectionCameras_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_ReflectionCameras_6, value);
	}

	inline static int32_t get_offset_of_m_ReflectionTexture_7() { return static_cast<int32_t>(offsetof(TCP2_PlanarReflection_t466005299, ___m_ReflectionTexture_7)); }
	inline RenderTexture_t2666733923 * get_m_ReflectionTexture_7() const { return ___m_ReflectionTexture_7; }
	inline RenderTexture_t2666733923 ** get_address_of_m_ReflectionTexture_7() { return &___m_ReflectionTexture_7; }
	inline void set_m_ReflectionTexture_7(RenderTexture_t2666733923 * value)
	{
		___m_ReflectionTexture_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_ReflectionTexture_7, value);
	}

	inline static int32_t get_offset_of_m_OldReflectionTextureSize_8() { return static_cast<int32_t>(offsetof(TCP2_PlanarReflection_t466005299, ___m_OldReflectionTextureSize_8)); }
	inline int32_t get_m_OldReflectionTextureSize_8() const { return ___m_OldReflectionTextureSize_8; }
	inline int32_t* get_address_of_m_OldReflectionTextureSize_8() { return &___m_OldReflectionTextureSize_8; }
	inline void set_m_OldReflectionTextureSize_8(int32_t value)
	{
		___m_OldReflectionTextureSize_8 = value;
	}
};

struct TCP2_PlanarReflection_t466005299_StaticFields
{
public:
	// System.Boolean TCP2_PlanarReflection::s_InsideRendering
	bool ___s_InsideRendering_9;

public:
	inline static int32_t get_offset_of_s_InsideRendering_9() { return static_cast<int32_t>(offsetof(TCP2_PlanarReflection_t466005299_StaticFields, ___s_InsideRendering_9)); }
	inline bool get_s_InsideRendering_9() const { return ___s_InsideRendering_9; }
	inline bool* get_address_of_s_InsideRendering_9() { return &___s_InsideRendering_9; }
	inline void set_s_InsideRendering_9(bool value)
	{
		___s_InsideRendering_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
