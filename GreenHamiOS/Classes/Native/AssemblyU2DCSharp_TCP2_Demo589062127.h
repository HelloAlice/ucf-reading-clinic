﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// TCP2_Demo_View
struct TCP2_Demo_View_t4250290717;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCP2_Demo
struct  TCP2_Demo_t589062127  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material[] TCP2_Demo::AffectedMaterials
	MaterialU5BU5D_t3123989686* ___AffectedMaterials_2;
	// UnityEngine.Texture2D[] TCP2_Demo::RampTextures
	Texture2DU5BU5D_t2724090252* ___RampTextures_3;
	// UnityEngine.GUISkin TCP2_Demo::GuiSkin
	GUISkin_t1436893342 * ___GuiSkin_4;
	// UnityEngine.Light TCP2_Demo::DirLight
	Light_t494725636 * ___DirLight_5;
	// UnityEngine.GameObject TCP2_Demo::Robot
	GameObject_t1756533147 * ___Robot_6;
	// UnityEngine.GameObject TCP2_Demo::Ethan
	GameObject_t1756533147 * ___Ethan_7;
	// System.Boolean TCP2_Demo::mUnityShader
	bool ___mUnityShader_8;
	// System.Boolean TCP2_Demo::mShaderSpecular
	bool ___mShaderSpecular_9;
	// System.Boolean TCP2_Demo::mShaderBump
	bool ___mShaderBump_10;
	// System.Boolean TCP2_Demo::mShaderReflection
	bool ___mShaderReflection_11;
	// System.Boolean TCP2_Demo::mShaderRim
	bool ___mShaderRim_12;
	// System.Boolean TCP2_Demo::mShaderRimOutline
	bool ___mShaderRimOutline_13;
	// System.Boolean TCP2_Demo::mShaderOutline
	bool ___mShaderOutline_14;
	// System.Single TCP2_Demo::mRimMin
	float ___mRimMin_15;
	// System.Single TCP2_Demo::mRimMax
	float ___mRimMax_16;
	// System.Boolean TCP2_Demo::mRampTextureFlag
	bool ___mRampTextureFlag_17;
	// UnityEngine.Texture2D TCP2_Demo::mRampTexture
	Texture2D_t3542995729 * ___mRampTexture_18;
	// System.Single TCP2_Demo::mRampSmoothing
	float ___mRampSmoothing_19;
	// System.Single TCP2_Demo::mLightRotationX
	float ___mLightRotationX_20;
	// System.Single TCP2_Demo::mLightRotationY
	float ___mLightRotationY_21;
	// System.Boolean TCP2_Demo::mViewRobot
	bool ___mViewRobot_22;
	// System.Boolean TCP2_Demo::mRobotOutlineNormals
	bool ___mRobotOutlineNormals_23;
	// TCP2_Demo_View TCP2_Demo::DemoView
	TCP2_Demo_View_t4250290717 * ___DemoView_24;

public:
	inline static int32_t get_offset_of_AffectedMaterials_2() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___AffectedMaterials_2)); }
	inline MaterialU5BU5D_t3123989686* get_AffectedMaterials_2() const { return ___AffectedMaterials_2; }
	inline MaterialU5BU5D_t3123989686** get_address_of_AffectedMaterials_2() { return &___AffectedMaterials_2; }
	inline void set_AffectedMaterials_2(MaterialU5BU5D_t3123989686* value)
	{
		___AffectedMaterials_2 = value;
		Il2CppCodeGenWriteBarrier(&___AffectedMaterials_2, value);
	}

	inline static int32_t get_offset_of_RampTextures_3() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___RampTextures_3)); }
	inline Texture2DU5BU5D_t2724090252* get_RampTextures_3() const { return ___RampTextures_3; }
	inline Texture2DU5BU5D_t2724090252** get_address_of_RampTextures_3() { return &___RampTextures_3; }
	inline void set_RampTextures_3(Texture2DU5BU5D_t2724090252* value)
	{
		___RampTextures_3 = value;
		Il2CppCodeGenWriteBarrier(&___RampTextures_3, value);
	}

	inline static int32_t get_offset_of_GuiSkin_4() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___GuiSkin_4)); }
	inline GUISkin_t1436893342 * get_GuiSkin_4() const { return ___GuiSkin_4; }
	inline GUISkin_t1436893342 ** get_address_of_GuiSkin_4() { return &___GuiSkin_4; }
	inline void set_GuiSkin_4(GUISkin_t1436893342 * value)
	{
		___GuiSkin_4 = value;
		Il2CppCodeGenWriteBarrier(&___GuiSkin_4, value);
	}

	inline static int32_t get_offset_of_DirLight_5() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___DirLight_5)); }
	inline Light_t494725636 * get_DirLight_5() const { return ___DirLight_5; }
	inline Light_t494725636 ** get_address_of_DirLight_5() { return &___DirLight_5; }
	inline void set_DirLight_5(Light_t494725636 * value)
	{
		___DirLight_5 = value;
		Il2CppCodeGenWriteBarrier(&___DirLight_5, value);
	}

	inline static int32_t get_offset_of_Robot_6() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___Robot_6)); }
	inline GameObject_t1756533147 * get_Robot_6() const { return ___Robot_6; }
	inline GameObject_t1756533147 ** get_address_of_Robot_6() { return &___Robot_6; }
	inline void set_Robot_6(GameObject_t1756533147 * value)
	{
		___Robot_6 = value;
		Il2CppCodeGenWriteBarrier(&___Robot_6, value);
	}

	inline static int32_t get_offset_of_Ethan_7() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___Ethan_7)); }
	inline GameObject_t1756533147 * get_Ethan_7() const { return ___Ethan_7; }
	inline GameObject_t1756533147 ** get_address_of_Ethan_7() { return &___Ethan_7; }
	inline void set_Ethan_7(GameObject_t1756533147 * value)
	{
		___Ethan_7 = value;
		Il2CppCodeGenWriteBarrier(&___Ethan_7, value);
	}

	inline static int32_t get_offset_of_mUnityShader_8() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mUnityShader_8)); }
	inline bool get_mUnityShader_8() const { return ___mUnityShader_8; }
	inline bool* get_address_of_mUnityShader_8() { return &___mUnityShader_8; }
	inline void set_mUnityShader_8(bool value)
	{
		___mUnityShader_8 = value;
	}

	inline static int32_t get_offset_of_mShaderSpecular_9() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mShaderSpecular_9)); }
	inline bool get_mShaderSpecular_9() const { return ___mShaderSpecular_9; }
	inline bool* get_address_of_mShaderSpecular_9() { return &___mShaderSpecular_9; }
	inline void set_mShaderSpecular_9(bool value)
	{
		___mShaderSpecular_9 = value;
	}

	inline static int32_t get_offset_of_mShaderBump_10() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mShaderBump_10)); }
	inline bool get_mShaderBump_10() const { return ___mShaderBump_10; }
	inline bool* get_address_of_mShaderBump_10() { return &___mShaderBump_10; }
	inline void set_mShaderBump_10(bool value)
	{
		___mShaderBump_10 = value;
	}

	inline static int32_t get_offset_of_mShaderReflection_11() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mShaderReflection_11)); }
	inline bool get_mShaderReflection_11() const { return ___mShaderReflection_11; }
	inline bool* get_address_of_mShaderReflection_11() { return &___mShaderReflection_11; }
	inline void set_mShaderReflection_11(bool value)
	{
		___mShaderReflection_11 = value;
	}

	inline static int32_t get_offset_of_mShaderRim_12() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mShaderRim_12)); }
	inline bool get_mShaderRim_12() const { return ___mShaderRim_12; }
	inline bool* get_address_of_mShaderRim_12() { return &___mShaderRim_12; }
	inline void set_mShaderRim_12(bool value)
	{
		___mShaderRim_12 = value;
	}

	inline static int32_t get_offset_of_mShaderRimOutline_13() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mShaderRimOutline_13)); }
	inline bool get_mShaderRimOutline_13() const { return ___mShaderRimOutline_13; }
	inline bool* get_address_of_mShaderRimOutline_13() { return &___mShaderRimOutline_13; }
	inline void set_mShaderRimOutline_13(bool value)
	{
		___mShaderRimOutline_13 = value;
	}

	inline static int32_t get_offset_of_mShaderOutline_14() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mShaderOutline_14)); }
	inline bool get_mShaderOutline_14() const { return ___mShaderOutline_14; }
	inline bool* get_address_of_mShaderOutline_14() { return &___mShaderOutline_14; }
	inline void set_mShaderOutline_14(bool value)
	{
		___mShaderOutline_14 = value;
	}

	inline static int32_t get_offset_of_mRimMin_15() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mRimMin_15)); }
	inline float get_mRimMin_15() const { return ___mRimMin_15; }
	inline float* get_address_of_mRimMin_15() { return &___mRimMin_15; }
	inline void set_mRimMin_15(float value)
	{
		___mRimMin_15 = value;
	}

	inline static int32_t get_offset_of_mRimMax_16() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mRimMax_16)); }
	inline float get_mRimMax_16() const { return ___mRimMax_16; }
	inline float* get_address_of_mRimMax_16() { return &___mRimMax_16; }
	inline void set_mRimMax_16(float value)
	{
		___mRimMax_16 = value;
	}

	inline static int32_t get_offset_of_mRampTextureFlag_17() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mRampTextureFlag_17)); }
	inline bool get_mRampTextureFlag_17() const { return ___mRampTextureFlag_17; }
	inline bool* get_address_of_mRampTextureFlag_17() { return &___mRampTextureFlag_17; }
	inline void set_mRampTextureFlag_17(bool value)
	{
		___mRampTextureFlag_17 = value;
	}

	inline static int32_t get_offset_of_mRampTexture_18() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mRampTexture_18)); }
	inline Texture2D_t3542995729 * get_mRampTexture_18() const { return ___mRampTexture_18; }
	inline Texture2D_t3542995729 ** get_address_of_mRampTexture_18() { return &___mRampTexture_18; }
	inline void set_mRampTexture_18(Texture2D_t3542995729 * value)
	{
		___mRampTexture_18 = value;
		Il2CppCodeGenWriteBarrier(&___mRampTexture_18, value);
	}

	inline static int32_t get_offset_of_mRampSmoothing_19() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mRampSmoothing_19)); }
	inline float get_mRampSmoothing_19() const { return ___mRampSmoothing_19; }
	inline float* get_address_of_mRampSmoothing_19() { return &___mRampSmoothing_19; }
	inline void set_mRampSmoothing_19(float value)
	{
		___mRampSmoothing_19 = value;
	}

	inline static int32_t get_offset_of_mLightRotationX_20() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mLightRotationX_20)); }
	inline float get_mLightRotationX_20() const { return ___mLightRotationX_20; }
	inline float* get_address_of_mLightRotationX_20() { return &___mLightRotationX_20; }
	inline void set_mLightRotationX_20(float value)
	{
		___mLightRotationX_20 = value;
	}

	inline static int32_t get_offset_of_mLightRotationY_21() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mLightRotationY_21)); }
	inline float get_mLightRotationY_21() const { return ___mLightRotationY_21; }
	inline float* get_address_of_mLightRotationY_21() { return &___mLightRotationY_21; }
	inline void set_mLightRotationY_21(float value)
	{
		___mLightRotationY_21 = value;
	}

	inline static int32_t get_offset_of_mViewRobot_22() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mViewRobot_22)); }
	inline bool get_mViewRobot_22() const { return ___mViewRobot_22; }
	inline bool* get_address_of_mViewRobot_22() { return &___mViewRobot_22; }
	inline void set_mViewRobot_22(bool value)
	{
		___mViewRobot_22 = value;
	}

	inline static int32_t get_offset_of_mRobotOutlineNormals_23() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___mRobotOutlineNormals_23)); }
	inline bool get_mRobotOutlineNormals_23() const { return ___mRobotOutlineNormals_23; }
	inline bool* get_address_of_mRobotOutlineNormals_23() { return &___mRobotOutlineNormals_23; }
	inline void set_mRobotOutlineNormals_23(bool value)
	{
		___mRobotOutlineNormals_23 = value;
	}

	inline static int32_t get_offset_of_DemoView_24() { return static_cast<int32_t>(offsetof(TCP2_Demo_t589062127, ___DemoView_24)); }
	inline TCP2_Demo_View_t4250290717 * get_DemoView_24() const { return ___DemoView_24; }
	inline TCP2_Demo_View_t4250290717 ** get_address_of_DemoView_24() { return &___DemoView_24; }
	inline void set_DemoView_24(TCP2_Demo_View_t4250290717 * value)
	{
		___DemoView_24 = value;
		Il2CppCodeGenWriteBarrier(&___DemoView_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
