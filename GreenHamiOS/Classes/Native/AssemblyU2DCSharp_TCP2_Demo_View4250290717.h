﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCP2_Demo_View
struct  TCP2_Demo_View_t4250290717  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TCP2_Demo_View::OrbitStrg
	float ___OrbitStrg_2;
	// System.Single TCP2_Demo_View::OrbitClamp
	float ___OrbitClamp_3;
	// System.Single TCP2_Demo_View::PanStrg
	float ___PanStrg_4;
	// System.Single TCP2_Demo_View::PanClamp
	float ___PanClamp_5;
	// System.Single TCP2_Demo_View::ZoomStrg
	float ___ZoomStrg_6;
	// System.Single TCP2_Demo_View::ZoomClamp
	float ___ZoomClamp_7;
	// System.Single TCP2_Demo_View::Decceleration
	float ___Decceleration_8;
	// UnityEngine.Transform TCP2_Demo_View::CharacterTransform
	Transform_t3275118058 * ___CharacterTransform_9;
	// UnityEngine.Vector3 TCP2_Demo_View::mouseDelta
	Vector3_t2243707580  ___mouseDelta_10;
	// UnityEngine.Vector3 TCP2_Demo_View::orbitAcceleration
	Vector3_t2243707580  ___orbitAcceleration_11;
	// UnityEngine.Vector3 TCP2_Demo_View::panAcceleration
	Vector3_t2243707580  ___panAcceleration_12;
	// UnityEngine.Vector3 TCP2_Demo_View::moveAcceleration
	Vector3_t2243707580  ___moveAcceleration_13;
	// System.Single TCP2_Demo_View::zoomAcceleration
	float ___zoomAcceleration_14;
	// UnityEngine.Vector3 TCP2_Demo_View::mResetCamPos
	Vector3_t2243707580  ___mResetCamPos_17;
	// UnityEngine.Vector3 TCP2_Demo_View::mResetCamRot
	Vector3_t2243707580  ___mResetCamRot_18;
	// System.Boolean TCP2_Demo_View::mMouseDown
	bool ___mMouseDown_19;

public:
	inline static int32_t get_offset_of_OrbitStrg_2() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___OrbitStrg_2)); }
	inline float get_OrbitStrg_2() const { return ___OrbitStrg_2; }
	inline float* get_address_of_OrbitStrg_2() { return &___OrbitStrg_2; }
	inline void set_OrbitStrg_2(float value)
	{
		___OrbitStrg_2 = value;
	}

	inline static int32_t get_offset_of_OrbitClamp_3() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___OrbitClamp_3)); }
	inline float get_OrbitClamp_3() const { return ___OrbitClamp_3; }
	inline float* get_address_of_OrbitClamp_3() { return &___OrbitClamp_3; }
	inline void set_OrbitClamp_3(float value)
	{
		___OrbitClamp_3 = value;
	}

	inline static int32_t get_offset_of_PanStrg_4() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___PanStrg_4)); }
	inline float get_PanStrg_4() const { return ___PanStrg_4; }
	inline float* get_address_of_PanStrg_4() { return &___PanStrg_4; }
	inline void set_PanStrg_4(float value)
	{
		___PanStrg_4 = value;
	}

	inline static int32_t get_offset_of_PanClamp_5() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___PanClamp_5)); }
	inline float get_PanClamp_5() const { return ___PanClamp_5; }
	inline float* get_address_of_PanClamp_5() { return &___PanClamp_5; }
	inline void set_PanClamp_5(float value)
	{
		___PanClamp_5 = value;
	}

	inline static int32_t get_offset_of_ZoomStrg_6() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___ZoomStrg_6)); }
	inline float get_ZoomStrg_6() const { return ___ZoomStrg_6; }
	inline float* get_address_of_ZoomStrg_6() { return &___ZoomStrg_6; }
	inline void set_ZoomStrg_6(float value)
	{
		___ZoomStrg_6 = value;
	}

	inline static int32_t get_offset_of_ZoomClamp_7() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___ZoomClamp_7)); }
	inline float get_ZoomClamp_7() const { return ___ZoomClamp_7; }
	inline float* get_address_of_ZoomClamp_7() { return &___ZoomClamp_7; }
	inline void set_ZoomClamp_7(float value)
	{
		___ZoomClamp_7 = value;
	}

	inline static int32_t get_offset_of_Decceleration_8() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___Decceleration_8)); }
	inline float get_Decceleration_8() const { return ___Decceleration_8; }
	inline float* get_address_of_Decceleration_8() { return &___Decceleration_8; }
	inline void set_Decceleration_8(float value)
	{
		___Decceleration_8 = value;
	}

	inline static int32_t get_offset_of_CharacterTransform_9() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___CharacterTransform_9)); }
	inline Transform_t3275118058 * get_CharacterTransform_9() const { return ___CharacterTransform_9; }
	inline Transform_t3275118058 ** get_address_of_CharacterTransform_9() { return &___CharacterTransform_9; }
	inline void set_CharacterTransform_9(Transform_t3275118058 * value)
	{
		___CharacterTransform_9 = value;
		Il2CppCodeGenWriteBarrier(&___CharacterTransform_9, value);
	}

	inline static int32_t get_offset_of_mouseDelta_10() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___mouseDelta_10)); }
	inline Vector3_t2243707580  get_mouseDelta_10() const { return ___mouseDelta_10; }
	inline Vector3_t2243707580 * get_address_of_mouseDelta_10() { return &___mouseDelta_10; }
	inline void set_mouseDelta_10(Vector3_t2243707580  value)
	{
		___mouseDelta_10 = value;
	}

	inline static int32_t get_offset_of_orbitAcceleration_11() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___orbitAcceleration_11)); }
	inline Vector3_t2243707580  get_orbitAcceleration_11() const { return ___orbitAcceleration_11; }
	inline Vector3_t2243707580 * get_address_of_orbitAcceleration_11() { return &___orbitAcceleration_11; }
	inline void set_orbitAcceleration_11(Vector3_t2243707580  value)
	{
		___orbitAcceleration_11 = value;
	}

	inline static int32_t get_offset_of_panAcceleration_12() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___panAcceleration_12)); }
	inline Vector3_t2243707580  get_panAcceleration_12() const { return ___panAcceleration_12; }
	inline Vector3_t2243707580 * get_address_of_panAcceleration_12() { return &___panAcceleration_12; }
	inline void set_panAcceleration_12(Vector3_t2243707580  value)
	{
		___panAcceleration_12 = value;
	}

	inline static int32_t get_offset_of_moveAcceleration_13() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___moveAcceleration_13)); }
	inline Vector3_t2243707580  get_moveAcceleration_13() const { return ___moveAcceleration_13; }
	inline Vector3_t2243707580 * get_address_of_moveAcceleration_13() { return &___moveAcceleration_13; }
	inline void set_moveAcceleration_13(Vector3_t2243707580  value)
	{
		___moveAcceleration_13 = value;
	}

	inline static int32_t get_offset_of_zoomAcceleration_14() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___zoomAcceleration_14)); }
	inline float get_zoomAcceleration_14() const { return ___zoomAcceleration_14; }
	inline float* get_address_of_zoomAcceleration_14() { return &___zoomAcceleration_14; }
	inline void set_zoomAcceleration_14(float value)
	{
		___zoomAcceleration_14 = value;
	}

	inline static int32_t get_offset_of_mResetCamPos_17() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___mResetCamPos_17)); }
	inline Vector3_t2243707580  get_mResetCamPos_17() const { return ___mResetCamPos_17; }
	inline Vector3_t2243707580 * get_address_of_mResetCamPos_17() { return &___mResetCamPos_17; }
	inline void set_mResetCamPos_17(Vector3_t2243707580  value)
	{
		___mResetCamPos_17 = value;
	}

	inline static int32_t get_offset_of_mResetCamRot_18() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___mResetCamRot_18)); }
	inline Vector3_t2243707580  get_mResetCamRot_18() const { return ___mResetCamRot_18; }
	inline Vector3_t2243707580 * get_address_of_mResetCamRot_18() { return &___mResetCamRot_18; }
	inline void set_mResetCamRot_18(Vector3_t2243707580  value)
	{
		___mResetCamRot_18 = value;
	}

	inline static int32_t get_offset_of_mMouseDown_19() { return static_cast<int32_t>(offsetof(TCP2_Demo_View_t4250290717, ___mMouseDown_19)); }
	inline bool get_mMouseDown_19() const { return ___mMouseDown_19; }
	inline bool* get_address_of_mMouseDown_19() { return &___mMouseDown_19; }
	inline void set_mMouseDown_19(bool value)
	{
		___mMouseDown_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
