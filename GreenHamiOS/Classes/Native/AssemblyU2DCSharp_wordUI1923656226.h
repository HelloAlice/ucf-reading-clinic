﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// wordUI
struct  wordUI_t1923656226  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform wordUI::panel
	RectTransform_t3349966182 * ___panel_2;
	// UnityEngine.UI.Image[] wordUI::btn
	ImageU5BU5D_t590162004* ___btn_3;
	// UnityEngine.RectTransform wordUI::center
	RectTransform_t3349966182 * ___center_4;
	// UnityEngine.UI.Image wordUI::samImg
	Image_t2042527209 * ___samImg_5;
	// UnityEngine.UI.Image wordUI::hatImg
	Image_t2042527209 * ___hatImg_6;
	// System.Boolean wordUI::debug
	bool ___debug_7;
	// System.Single[] wordUI::distanceToCenter
	SingleU5BU5D_t577127397* ___distanceToCenter_8;
	// System.Boolean wordUI::dragging
	bool ___dragging_9;
	// System.Int32 wordUI::btnDistance
	int32_t ___btnDistance_10;
	// System.Int32 wordUI::minButtonNum
	int32_t ___minButtonNum_11;
	// UnityEngine.GameObject wordUI::scrollPanel
	GameObject_t1756533147 * ___scrollPanel_12;

public:
	inline static int32_t get_offset_of_panel_2() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___panel_2)); }
	inline RectTransform_t3349966182 * get_panel_2() const { return ___panel_2; }
	inline RectTransform_t3349966182 ** get_address_of_panel_2() { return &___panel_2; }
	inline void set_panel_2(RectTransform_t3349966182 * value)
	{
		___panel_2 = value;
		Il2CppCodeGenWriteBarrier(&___panel_2, value);
	}

	inline static int32_t get_offset_of_btn_3() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___btn_3)); }
	inline ImageU5BU5D_t590162004* get_btn_3() const { return ___btn_3; }
	inline ImageU5BU5D_t590162004** get_address_of_btn_3() { return &___btn_3; }
	inline void set_btn_3(ImageU5BU5D_t590162004* value)
	{
		___btn_3 = value;
		Il2CppCodeGenWriteBarrier(&___btn_3, value);
	}

	inline static int32_t get_offset_of_center_4() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___center_4)); }
	inline RectTransform_t3349966182 * get_center_4() const { return ___center_4; }
	inline RectTransform_t3349966182 ** get_address_of_center_4() { return &___center_4; }
	inline void set_center_4(RectTransform_t3349966182 * value)
	{
		___center_4 = value;
		Il2CppCodeGenWriteBarrier(&___center_4, value);
	}

	inline static int32_t get_offset_of_samImg_5() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___samImg_5)); }
	inline Image_t2042527209 * get_samImg_5() const { return ___samImg_5; }
	inline Image_t2042527209 ** get_address_of_samImg_5() { return &___samImg_5; }
	inline void set_samImg_5(Image_t2042527209 * value)
	{
		___samImg_5 = value;
		Il2CppCodeGenWriteBarrier(&___samImg_5, value);
	}

	inline static int32_t get_offset_of_hatImg_6() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___hatImg_6)); }
	inline Image_t2042527209 * get_hatImg_6() const { return ___hatImg_6; }
	inline Image_t2042527209 ** get_address_of_hatImg_6() { return &___hatImg_6; }
	inline void set_hatImg_6(Image_t2042527209 * value)
	{
		___hatImg_6 = value;
		Il2CppCodeGenWriteBarrier(&___hatImg_6, value);
	}

	inline static int32_t get_offset_of_debug_7() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___debug_7)); }
	inline bool get_debug_7() const { return ___debug_7; }
	inline bool* get_address_of_debug_7() { return &___debug_7; }
	inline void set_debug_7(bool value)
	{
		___debug_7 = value;
	}

	inline static int32_t get_offset_of_distanceToCenter_8() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___distanceToCenter_8)); }
	inline SingleU5BU5D_t577127397* get_distanceToCenter_8() const { return ___distanceToCenter_8; }
	inline SingleU5BU5D_t577127397** get_address_of_distanceToCenter_8() { return &___distanceToCenter_8; }
	inline void set_distanceToCenter_8(SingleU5BU5D_t577127397* value)
	{
		___distanceToCenter_8 = value;
		Il2CppCodeGenWriteBarrier(&___distanceToCenter_8, value);
	}

	inline static int32_t get_offset_of_dragging_9() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___dragging_9)); }
	inline bool get_dragging_9() const { return ___dragging_9; }
	inline bool* get_address_of_dragging_9() { return &___dragging_9; }
	inline void set_dragging_9(bool value)
	{
		___dragging_9 = value;
	}

	inline static int32_t get_offset_of_btnDistance_10() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___btnDistance_10)); }
	inline int32_t get_btnDistance_10() const { return ___btnDistance_10; }
	inline int32_t* get_address_of_btnDistance_10() { return &___btnDistance_10; }
	inline void set_btnDistance_10(int32_t value)
	{
		___btnDistance_10 = value;
	}

	inline static int32_t get_offset_of_minButtonNum_11() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___minButtonNum_11)); }
	inline int32_t get_minButtonNum_11() const { return ___minButtonNum_11; }
	inline int32_t* get_address_of_minButtonNum_11() { return &___minButtonNum_11; }
	inline void set_minButtonNum_11(int32_t value)
	{
		___minButtonNum_11 = value;
	}

	inline static int32_t get_offset_of_scrollPanel_12() { return static_cast<int32_t>(offsetof(wordUI_t1923656226, ___scrollPanel_12)); }
	inline GameObject_t1756533147 * get_scrollPanel_12() const { return ___scrollPanel_12; }
	inline GameObject_t1756533147 ** get_address_of_scrollPanel_12() { return &___scrollPanel_12; }
	inline void set_scrollPanel_12(GameObject_t1756533147 * value)
	{
		___scrollPanel_12 = value;
		Il2CppCodeGenWriteBarrier(&___scrollPanel_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
