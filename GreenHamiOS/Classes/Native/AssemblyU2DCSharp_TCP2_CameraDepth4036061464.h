﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCP2_CameraDepth
struct  TCP2_CameraDepth_t4036061464  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TCP2_CameraDepth::RenderDepth
	bool ___RenderDepth_2;

public:
	inline static int32_t get_offset_of_RenderDepth_2() { return static_cast<int32_t>(offsetof(TCP2_CameraDepth_t4036061464, ___RenderDepth_2)); }
	inline bool get_RenderDepth_2() const { return ___RenderDepth_2; }
	inline bool* get_address_of_RenderDepth_2() { return &___RenderDepth_2; }
	inline void set_RenderDepth_2(bool value)
	{
		___RenderDepth_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
