﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// aliceHandler
struct  aliceHandler_t4282217804  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject aliceHandler::laugh
	GameObject_t1756533147 * ___laugh_2;
	// UnityEngine.GameObject aliceHandler::fly
	GameObject_t1756533147 * ___fly_3;
	// UnityEngine.GameObject aliceHandler::celebration
	GameObject_t1756533147 * ___celebration_4;
	// UnityEngine.GameObject aliceHandler::wave
	GameObject_t1756533147 * ___wave_5;

public:
	inline static int32_t get_offset_of_laugh_2() { return static_cast<int32_t>(offsetof(aliceHandler_t4282217804, ___laugh_2)); }
	inline GameObject_t1756533147 * get_laugh_2() const { return ___laugh_2; }
	inline GameObject_t1756533147 ** get_address_of_laugh_2() { return &___laugh_2; }
	inline void set_laugh_2(GameObject_t1756533147 * value)
	{
		___laugh_2 = value;
		Il2CppCodeGenWriteBarrier(&___laugh_2, value);
	}

	inline static int32_t get_offset_of_fly_3() { return static_cast<int32_t>(offsetof(aliceHandler_t4282217804, ___fly_3)); }
	inline GameObject_t1756533147 * get_fly_3() const { return ___fly_3; }
	inline GameObject_t1756533147 ** get_address_of_fly_3() { return &___fly_3; }
	inline void set_fly_3(GameObject_t1756533147 * value)
	{
		___fly_3 = value;
		Il2CppCodeGenWriteBarrier(&___fly_3, value);
	}

	inline static int32_t get_offset_of_celebration_4() { return static_cast<int32_t>(offsetof(aliceHandler_t4282217804, ___celebration_4)); }
	inline GameObject_t1756533147 * get_celebration_4() const { return ___celebration_4; }
	inline GameObject_t1756533147 ** get_address_of_celebration_4() { return &___celebration_4; }
	inline void set_celebration_4(GameObject_t1756533147 * value)
	{
		___celebration_4 = value;
		Il2CppCodeGenWriteBarrier(&___celebration_4, value);
	}

	inline static int32_t get_offset_of_wave_5() { return static_cast<int32_t>(offsetof(aliceHandler_t4282217804, ___wave_5)); }
	inline GameObject_t1756533147 * get_wave_5() const { return ___wave_5; }
	inline GameObject_t1756533147 ** get_address_of_wave_5() { return &___wave_5; }
	inline void set_wave_5(GameObject_t1756533147 * value)
	{
		___wave_5 = value;
		Il2CppCodeGenWriteBarrier(&___wave_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
