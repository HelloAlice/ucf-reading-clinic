﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCP2_Demo_PBS/SkyboxSetting
struct  SkyboxSetting_t1304978090  : public Il2CppObject
{
public:
	// UnityEngine.Material TCP2_Demo_PBS/SkyboxSetting::SkyMaterial
	Material_t193706927 * ___SkyMaterial_0;
	// UnityEngine.Color TCP2_Demo_PBS/SkyboxSetting::lightColor
	Color_t2020392075  ___lightColor_1;
	// UnityEngine.Vector3 TCP2_Demo_PBS/SkyboxSetting::DirLightEuler
	Vector3_t2243707580  ___DirLightEuler_2;

public:
	inline static int32_t get_offset_of_SkyMaterial_0() { return static_cast<int32_t>(offsetof(SkyboxSetting_t1304978090, ___SkyMaterial_0)); }
	inline Material_t193706927 * get_SkyMaterial_0() const { return ___SkyMaterial_0; }
	inline Material_t193706927 ** get_address_of_SkyMaterial_0() { return &___SkyMaterial_0; }
	inline void set_SkyMaterial_0(Material_t193706927 * value)
	{
		___SkyMaterial_0 = value;
		Il2CppCodeGenWriteBarrier(&___SkyMaterial_0, value);
	}

	inline static int32_t get_offset_of_lightColor_1() { return static_cast<int32_t>(offsetof(SkyboxSetting_t1304978090, ___lightColor_1)); }
	inline Color_t2020392075  get_lightColor_1() const { return ___lightColor_1; }
	inline Color_t2020392075 * get_address_of_lightColor_1() { return &___lightColor_1; }
	inline void set_lightColor_1(Color_t2020392075  value)
	{
		___lightColor_1 = value;
	}

	inline static int32_t get_offset_of_DirLightEuler_2() { return static_cast<int32_t>(offsetof(SkyboxSetting_t1304978090, ___DirLightEuler_2)); }
	inline Vector3_t2243707580  get_DirLightEuler_2() const { return ___DirLightEuler_2; }
	inline Vector3_t2243707580 * get_address_of_DirLightEuler_2() { return &___DirLightEuler_2; }
	inline void set_DirLightEuler_2(Vector3_t2243707580  value)
	{
		___DirLightEuler_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
