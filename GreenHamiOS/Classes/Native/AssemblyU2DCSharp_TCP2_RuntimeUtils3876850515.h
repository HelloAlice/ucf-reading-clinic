﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String[]>
struct List_1_t1011507104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCP2_RuntimeUtils
struct  TCP2_RuntimeUtils_t3876850515  : public Il2CppObject
{
public:

public:
};

struct TCP2_RuntimeUtils_t3876850515_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String[]> TCP2_RuntimeUtils::ShaderVariants
	List_1_t1011507104 * ___ShaderVariants_4;

public:
	inline static int32_t get_offset_of_ShaderVariants_4() { return static_cast<int32_t>(offsetof(TCP2_RuntimeUtils_t3876850515_StaticFields, ___ShaderVariants_4)); }
	inline List_1_t1011507104 * get_ShaderVariants_4() const { return ___ShaderVariants_4; }
	inline List_1_t1011507104 ** get_address_of_ShaderVariants_4() { return &___ShaderVariants_4; }
	inline void set_ShaderVariants_4(List_1_t1011507104 * value)
	{
		___ShaderVariants_4 = value;
		Il2CppCodeGenWriteBarrier(&___ShaderVariants_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
