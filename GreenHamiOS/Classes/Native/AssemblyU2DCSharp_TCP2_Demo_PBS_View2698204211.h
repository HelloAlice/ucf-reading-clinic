﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCP2_Demo_PBS_View
struct  TCP2_Demo_PBS_View_t2698204211  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform TCP2_Demo_PBS_View::Pivot
	Transform_t3275118058 * ___Pivot_2;
	// System.Single TCP2_Demo_PBS_View::OrbitStrg
	float ___OrbitStrg_3;
	// System.Single TCP2_Demo_PBS_View::OrbitClamp
	float ___OrbitClamp_4;
	// System.Single TCP2_Demo_PBS_View::PanStrg
	float ___PanStrg_5;
	// System.Single TCP2_Demo_PBS_View::PanClamp
	float ___PanClamp_6;
	// System.Single TCP2_Demo_PBS_View::yMin
	float ___yMin_7;
	// System.Single TCP2_Demo_PBS_View::yMax
	float ___yMax_8;
	// System.Single TCP2_Demo_PBS_View::ZoomStrg
	float ___ZoomStrg_9;
	// System.Single TCP2_Demo_PBS_View::ZoomClamp
	float ___ZoomClamp_10;
	// System.Single TCP2_Demo_PBS_View::ZoomDistMin
	float ___ZoomDistMin_11;
	// System.Single TCP2_Demo_PBS_View::ZoomDistMax
	float ___ZoomDistMax_12;
	// System.Single TCP2_Demo_PBS_View::Decceleration
	float ___Decceleration_13;
	// UnityEngine.Rect TCP2_Demo_PBS_View::ignoreMouseRect
	Rect_t3681755626  ___ignoreMouseRect_14;
	// UnityEngine.Vector3 TCP2_Demo_PBS_View::mouseDelta
	Vector3_t2243707580  ___mouseDelta_15;
	// UnityEngine.Vector3 TCP2_Demo_PBS_View::orbitAcceleration
	Vector3_t2243707580  ___orbitAcceleration_16;
	// UnityEngine.Vector3 TCP2_Demo_PBS_View::panAcceleration
	Vector3_t2243707580  ___panAcceleration_17;
	// UnityEngine.Vector3 TCP2_Demo_PBS_View::moveAcceleration
	Vector3_t2243707580  ___moveAcceleration_18;
	// System.Single TCP2_Demo_PBS_View::zoomAcceleration
	float ___zoomAcceleration_19;
	// UnityEngine.Vector3 TCP2_Demo_PBS_View::mResetCamPos
	Vector3_t2243707580  ___mResetCamPos_22;
	// UnityEngine.Vector3 TCP2_Demo_PBS_View::mResetPivotPos
	Vector3_t2243707580  ___mResetPivotPos_23;
	// UnityEngine.Vector3 TCP2_Demo_PBS_View::mResetCamRot
	Vector3_t2243707580  ___mResetCamRot_24;
	// UnityEngine.Vector3 TCP2_Demo_PBS_View::mResetPivotRot
	Vector3_t2243707580  ___mResetPivotRot_25;
	// System.Boolean TCP2_Demo_PBS_View::leftMouseHeld
	bool ___leftMouseHeld_26;
	// System.Boolean TCP2_Demo_PBS_View::rightMouseHeld
	bool ___rightMouseHeld_27;
	// System.Boolean TCP2_Demo_PBS_View::middleMouseHeld
	bool ___middleMouseHeld_28;

public:
	inline static int32_t get_offset_of_Pivot_2() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___Pivot_2)); }
	inline Transform_t3275118058 * get_Pivot_2() const { return ___Pivot_2; }
	inline Transform_t3275118058 ** get_address_of_Pivot_2() { return &___Pivot_2; }
	inline void set_Pivot_2(Transform_t3275118058 * value)
	{
		___Pivot_2 = value;
		Il2CppCodeGenWriteBarrier(&___Pivot_2, value);
	}

	inline static int32_t get_offset_of_OrbitStrg_3() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___OrbitStrg_3)); }
	inline float get_OrbitStrg_3() const { return ___OrbitStrg_3; }
	inline float* get_address_of_OrbitStrg_3() { return &___OrbitStrg_3; }
	inline void set_OrbitStrg_3(float value)
	{
		___OrbitStrg_3 = value;
	}

	inline static int32_t get_offset_of_OrbitClamp_4() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___OrbitClamp_4)); }
	inline float get_OrbitClamp_4() const { return ___OrbitClamp_4; }
	inline float* get_address_of_OrbitClamp_4() { return &___OrbitClamp_4; }
	inline void set_OrbitClamp_4(float value)
	{
		___OrbitClamp_4 = value;
	}

	inline static int32_t get_offset_of_PanStrg_5() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___PanStrg_5)); }
	inline float get_PanStrg_5() const { return ___PanStrg_5; }
	inline float* get_address_of_PanStrg_5() { return &___PanStrg_5; }
	inline void set_PanStrg_5(float value)
	{
		___PanStrg_5 = value;
	}

	inline static int32_t get_offset_of_PanClamp_6() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___PanClamp_6)); }
	inline float get_PanClamp_6() const { return ___PanClamp_6; }
	inline float* get_address_of_PanClamp_6() { return &___PanClamp_6; }
	inline void set_PanClamp_6(float value)
	{
		___PanClamp_6 = value;
	}

	inline static int32_t get_offset_of_yMin_7() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___yMin_7)); }
	inline float get_yMin_7() const { return ___yMin_7; }
	inline float* get_address_of_yMin_7() { return &___yMin_7; }
	inline void set_yMin_7(float value)
	{
		___yMin_7 = value;
	}

	inline static int32_t get_offset_of_yMax_8() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___yMax_8)); }
	inline float get_yMax_8() const { return ___yMax_8; }
	inline float* get_address_of_yMax_8() { return &___yMax_8; }
	inline void set_yMax_8(float value)
	{
		___yMax_8 = value;
	}

	inline static int32_t get_offset_of_ZoomStrg_9() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___ZoomStrg_9)); }
	inline float get_ZoomStrg_9() const { return ___ZoomStrg_9; }
	inline float* get_address_of_ZoomStrg_9() { return &___ZoomStrg_9; }
	inline void set_ZoomStrg_9(float value)
	{
		___ZoomStrg_9 = value;
	}

	inline static int32_t get_offset_of_ZoomClamp_10() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___ZoomClamp_10)); }
	inline float get_ZoomClamp_10() const { return ___ZoomClamp_10; }
	inline float* get_address_of_ZoomClamp_10() { return &___ZoomClamp_10; }
	inline void set_ZoomClamp_10(float value)
	{
		___ZoomClamp_10 = value;
	}

	inline static int32_t get_offset_of_ZoomDistMin_11() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___ZoomDistMin_11)); }
	inline float get_ZoomDistMin_11() const { return ___ZoomDistMin_11; }
	inline float* get_address_of_ZoomDistMin_11() { return &___ZoomDistMin_11; }
	inline void set_ZoomDistMin_11(float value)
	{
		___ZoomDistMin_11 = value;
	}

	inline static int32_t get_offset_of_ZoomDistMax_12() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___ZoomDistMax_12)); }
	inline float get_ZoomDistMax_12() const { return ___ZoomDistMax_12; }
	inline float* get_address_of_ZoomDistMax_12() { return &___ZoomDistMax_12; }
	inline void set_ZoomDistMax_12(float value)
	{
		___ZoomDistMax_12 = value;
	}

	inline static int32_t get_offset_of_Decceleration_13() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___Decceleration_13)); }
	inline float get_Decceleration_13() const { return ___Decceleration_13; }
	inline float* get_address_of_Decceleration_13() { return &___Decceleration_13; }
	inline void set_Decceleration_13(float value)
	{
		___Decceleration_13 = value;
	}

	inline static int32_t get_offset_of_ignoreMouseRect_14() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___ignoreMouseRect_14)); }
	inline Rect_t3681755626  get_ignoreMouseRect_14() const { return ___ignoreMouseRect_14; }
	inline Rect_t3681755626 * get_address_of_ignoreMouseRect_14() { return &___ignoreMouseRect_14; }
	inline void set_ignoreMouseRect_14(Rect_t3681755626  value)
	{
		___ignoreMouseRect_14 = value;
	}

	inline static int32_t get_offset_of_mouseDelta_15() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___mouseDelta_15)); }
	inline Vector3_t2243707580  get_mouseDelta_15() const { return ___mouseDelta_15; }
	inline Vector3_t2243707580 * get_address_of_mouseDelta_15() { return &___mouseDelta_15; }
	inline void set_mouseDelta_15(Vector3_t2243707580  value)
	{
		___mouseDelta_15 = value;
	}

	inline static int32_t get_offset_of_orbitAcceleration_16() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___orbitAcceleration_16)); }
	inline Vector3_t2243707580  get_orbitAcceleration_16() const { return ___orbitAcceleration_16; }
	inline Vector3_t2243707580 * get_address_of_orbitAcceleration_16() { return &___orbitAcceleration_16; }
	inline void set_orbitAcceleration_16(Vector3_t2243707580  value)
	{
		___orbitAcceleration_16 = value;
	}

	inline static int32_t get_offset_of_panAcceleration_17() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___panAcceleration_17)); }
	inline Vector3_t2243707580  get_panAcceleration_17() const { return ___panAcceleration_17; }
	inline Vector3_t2243707580 * get_address_of_panAcceleration_17() { return &___panAcceleration_17; }
	inline void set_panAcceleration_17(Vector3_t2243707580  value)
	{
		___panAcceleration_17 = value;
	}

	inline static int32_t get_offset_of_moveAcceleration_18() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___moveAcceleration_18)); }
	inline Vector3_t2243707580  get_moveAcceleration_18() const { return ___moveAcceleration_18; }
	inline Vector3_t2243707580 * get_address_of_moveAcceleration_18() { return &___moveAcceleration_18; }
	inline void set_moveAcceleration_18(Vector3_t2243707580  value)
	{
		___moveAcceleration_18 = value;
	}

	inline static int32_t get_offset_of_zoomAcceleration_19() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___zoomAcceleration_19)); }
	inline float get_zoomAcceleration_19() const { return ___zoomAcceleration_19; }
	inline float* get_address_of_zoomAcceleration_19() { return &___zoomAcceleration_19; }
	inline void set_zoomAcceleration_19(float value)
	{
		___zoomAcceleration_19 = value;
	}

	inline static int32_t get_offset_of_mResetCamPos_22() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___mResetCamPos_22)); }
	inline Vector3_t2243707580  get_mResetCamPos_22() const { return ___mResetCamPos_22; }
	inline Vector3_t2243707580 * get_address_of_mResetCamPos_22() { return &___mResetCamPos_22; }
	inline void set_mResetCamPos_22(Vector3_t2243707580  value)
	{
		___mResetCamPos_22 = value;
	}

	inline static int32_t get_offset_of_mResetPivotPos_23() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___mResetPivotPos_23)); }
	inline Vector3_t2243707580  get_mResetPivotPos_23() const { return ___mResetPivotPos_23; }
	inline Vector3_t2243707580 * get_address_of_mResetPivotPos_23() { return &___mResetPivotPos_23; }
	inline void set_mResetPivotPos_23(Vector3_t2243707580  value)
	{
		___mResetPivotPos_23 = value;
	}

	inline static int32_t get_offset_of_mResetCamRot_24() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___mResetCamRot_24)); }
	inline Vector3_t2243707580  get_mResetCamRot_24() const { return ___mResetCamRot_24; }
	inline Vector3_t2243707580 * get_address_of_mResetCamRot_24() { return &___mResetCamRot_24; }
	inline void set_mResetCamRot_24(Vector3_t2243707580  value)
	{
		___mResetCamRot_24 = value;
	}

	inline static int32_t get_offset_of_mResetPivotRot_25() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___mResetPivotRot_25)); }
	inline Vector3_t2243707580  get_mResetPivotRot_25() const { return ___mResetPivotRot_25; }
	inline Vector3_t2243707580 * get_address_of_mResetPivotRot_25() { return &___mResetPivotRot_25; }
	inline void set_mResetPivotRot_25(Vector3_t2243707580  value)
	{
		___mResetPivotRot_25 = value;
	}

	inline static int32_t get_offset_of_leftMouseHeld_26() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___leftMouseHeld_26)); }
	inline bool get_leftMouseHeld_26() const { return ___leftMouseHeld_26; }
	inline bool* get_address_of_leftMouseHeld_26() { return &___leftMouseHeld_26; }
	inline void set_leftMouseHeld_26(bool value)
	{
		___leftMouseHeld_26 = value;
	}

	inline static int32_t get_offset_of_rightMouseHeld_27() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___rightMouseHeld_27)); }
	inline bool get_rightMouseHeld_27() const { return ___rightMouseHeld_27; }
	inline bool* get_address_of_rightMouseHeld_27() { return &___rightMouseHeld_27; }
	inline void set_rightMouseHeld_27(bool value)
	{
		___rightMouseHeld_27 = value;
	}

	inline static int32_t get_offset_of_middleMouseHeld_28() { return static_cast<int32_t>(offsetof(TCP2_Demo_PBS_View_t2698204211, ___middleMouseHeld_28)); }
	inline bool get_middleMouseHeld_28() const { return ___middleMouseHeld_28; }
	inline bool* get_address_of_middleMouseHeld_28() { return &___middleMouseHeld_28; }
	inline void set_middleMouseHeld_28(bool value)
	{
		___middleMouseHeld_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
